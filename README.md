# go-auth-admin

#### 介绍
 - 多系统管理后台菜单和按钮
 - 用户登陆认证，单点登陆
 - 基于RBAC菜单和按钮权限
 - 组织管理
 - ODAC 组织数据权限管理。

#### 代码组织学习一下其他大型项目
 - /apis  pg文件，grpc接口定义处
 - /httpapis http request form, response 接口定义处