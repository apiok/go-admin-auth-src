package main

import (
	"fmt"
	"go-admin-auth/conf"
	_ "go-admin-auth/conf"
	"go-admin-auth/router"
	"log"
	"net"
	"net/http"
)

func main() {
	// graceful restart
	// pprof
	// app init
	host := conf.GetEnvDft("APP_HOST", "")
	port := conf.GetEnvDft("APP_PORT", ":8100")

	listener, err := net.Listen("tcp", fmt.Sprintf("%s%s", host, port))
	if err != nil {
		log.Fatal("init listener failure:", err)
	}
	log.Println("listen at:", fmt.Sprintf("%s%s", host, port))

	err = http.Serve(listener, router.AppServer)
	if err != nil {
		log.Fatal("http.Serve failure:", err)
	}

}
