.PHONY:test deploy build build-linux  build-windows publish publish-config

GO_ADMIN_AUTH_PATH=../go-admin-auth/

test:
	echo "need an action"

deploy: build-linux publish

build: tar-themes build-darwin build-linux build-windows
	echo "build"

#Go语言跨平台跨架构编译
#https://baijiahao.baidu.com/s?id=1699090815871480487&wfr=spider&for=pc
#Go build 不同系统下的可执行文件
#https://www.cnblogs.com/ztshuai/p/12632400.html
build-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/go-admin-auth go-admin-auth

build-windows:
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64  go build -o bin/go-admin-auth.exe go-admin-auth

build-darwin:
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o bin/go-admin-auth-mac go-admin-auth

tar-themes:
	tar czf ./bin/themes.tar.gz ./themes

publish-git:
	scp ./bin/go-rest* $(GO_ADMIN_AUTH_PATH)
	scp ./env.example $(GO_ADMIN_AUTH_PATH)

publish-tx:
	scp ./bin/go-admin-auth txyun:/data/go-project/go-admin-auth/
	scp ./bin/themes.tar.gz txyun:/data/go-project/go-admin-auth/
	ssh txyun "tar xzvf /data/go-project/go-admin-auth/themes.tar.gz --directory=/data/go-project/go-admin-auth/"