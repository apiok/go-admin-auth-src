SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `system_id` int NOT NULL DEFAULT '0' COMMENT '子系统ID',
  `parent_id` int NOT NULL DEFAULT '0' COMMENT '上级菜单ID',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单名',
  `level` int NOT NULL DEFAULT '0' COMMENT '菜单级别',
  `path` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单路径',
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '对应的uri',
  `target_tab` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '页面上显示的tabID',
  `status` int NOT NULL DEFAULT '1' COMMENT '1:有效 2:禁用',
  `order_no` int NOT NULL DEFAULT '0' COMMENT '菜单序号',
  `is_show` int NOT NULL DEFAULT '1' COMMENT '是否隐藏型的菜单，1正常，2隐常',
  `create_uid` int NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_uid` int NOT NULL DEFAULT '0',
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `sys_id_pid_name` (`system_id`,`parent_id`,`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
BEGIN;
INSERT INTO `t_menu` VALUES (1, 1, 0, '用户组织角色管理', 0, '/', '/', '', 1, 11, 1, 1, '2018-03-21 10:14:31', 1, '2018-03-21 11:46:59');
INSERT INTO `t_menu` VALUES (2, 1, 0, '系统菜单管理', 0, '/', '/', '', 1, 12, 1, 1, '2018-03-21 10:14:48', 1, '2018-03-21 11:47:12');
INSERT INTO `t_menu` VALUES (3, 1, 0, '系统管理', 0, '/', '/', '', 1, 13, 1, 1, '2018-03-21 10:15:05', 1, '2018-03-21 11:47:22');
INSERT INTO `t_menu` VALUES (4, 1, 1, '组织管理', 1, '/1/', '/org/list', 'id_org_list', 1, 111, 1, 1, '2018-03-21 10:16:23', 1, '2018-03-21 11:47:35');
INSERT INTO `t_menu` VALUES (5, 1, 1, '用户管理', 1, '/1/', '/user/list', 'id_user_list', 1, 112, 1, 1, '2018-03-21 10:17:13', 1, '2018-03-21 11:47:42');
INSERT INTO `t_menu` VALUES (6, 1, 1, '角色管理', 1, '/1/', '/role/list', 'id_role_list', 1, 113, 1, 1, '2018-03-21 10:17:51', 1, '2018-03-21 11:47:58');
INSERT INTO `t_menu` VALUES (7, 1, 2, '菜单管理', 1, '/2/', '/menu/list', 'id_menu_list', 1, 121, 1, 1, '2018-03-21 10:19:07', 1, '2018-03-21 11:48:08');
INSERT INTO `t_menu` VALUES (8, 1, 2, '操作管理', 1, '/2/', '/opera/list', 'id_opera_list', 1, 122, 1, 1, '2018-03-21 10:30:15', 1, '2018-03-21 11:48:14');
INSERT INTO `t_menu` VALUES (9, 1, 3, '子系统管理', 1, '/3/', '/subsystem/list', 'id_system_list', 1, 131, 1, 1, '2018-03-21 10:31:10', 1, '2018-03-21 11:48:20');
INSERT INTO `t_menu` VALUES (93, 7, 0, '产品管理', 0, '/', '/', '', 1, 71, 1, 1, '2019-10-24 14:09:48', 1, '2019-10-24 14:19:09');
INSERT INTO `t_menu` VALUES (94, 7, 93, '订单列表', 1, '/93/', '/order/list', '', 1, 711, 1, 1, '2019-10-24 14:20:12', 1, '2019-10-24 14:20:12');
INSERT INTO `t_menu` VALUES (95, 7, 93, '产品分类列表', 1, '/93/', '/classify/list', '', 1, 712, 1, 1, '2019-10-24 14:20:42', 1, '2019-10-24 14:20:42');
INSERT INTO `t_menu` VALUES (96, 7, 93, '标签列表', 1, '/93/', '/tag/list', '', 1, 713, 1, 1, '2019-10-24 14:21:07', 1, '2019-10-24 14:21:07');
INSERT INTO `t_menu` VALUES (200, 7, 93, '支出账单', 1, '/93/', '/bill/list', '', 1, 717, 1, 22, '2020-11-20 14:47:00', 22, '2020-11-20 14:47:00');
INSERT INTO `t_menu` VALUES (230, 7, 0, '统计数据', 0, '/', '/', '', 1, 91, 1, 1, '2021-04-27 17:03:55', 1, '2021-04-27 17:03:55');
INSERT INTO `t_menu` VALUES (234, 7, 230, '产品销售率', 1, '/230/', '/statistics/selling/list', '', 1, 911, 1, 1, '2021-04-27 17:07:09', 1, '2021-05-11 18:46:36');
COMMIT;

-- ----------------------------
-- Table structure for t_operation
-- ----------------------------
DROP TABLE IF EXISTS `t_operation`;
CREATE TABLE `t_operation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `system_id` int NOT NULL DEFAULT '0' COMMENT '子系统ID',
  `parent_id` int NOT NULL DEFAULT '0' COMMENT '一级菜单ID',
  `menu_id` int NOT NULL DEFAULT '0' COMMENT '菜单ID',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作名称',
  `code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作编号,此编号对应页面class, 用来控制按钮是否显示，也对应后台接口操作编号，校验用户是否有此操作权限',
  `status` int NOT NULL DEFAULT '1' COMMENT '1:有效 2:禁用',
  `order_no` int NOT NULL DEFAULT '1' COMMENT '排序',
  `create_uid` int NOT NULL DEFAULT '0' COMMENT '创建人id',
  `create_time` datetime NOT NULL,
  `update_uid` int NOT NULL DEFAULT '0' COMMENT '更新用户id',
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_operation
-- ----------------------------
BEGIN;
INSERT INTO `t_operation` VALUES (103, 7, 93, 200, '全部数据', 'data-all', 1, 101, 1, '2022-03-27 12:08:57', 1, '2022-03-27 12:09:02');
INSERT INTO `t_operation` VALUES (115, 7, 93, 95, '添加', 'classify-add', 1, 0, 38, '2022-03-27 12:08:57', 38, '2022-03-27 12:09:02');
INSERT INTO `t_operation` VALUES (116, 7, 93, 95, '编辑', 'classify-edit', 1, 0, 38, '2022-03-27 12:08:57', 38, '2022-03-27 12:09:02');
INSERT INTO `t_operation` VALUES (118, 7, 93, 97, '编辑', 'a-edit', 1, 0, 38, '2022-03-27 12:08:57', 38, '2022-03-27 12:09:02');
INSERT INTO `t_operation` VALUES (124, 7, 93, 200, '编辑', 'b-edit', 1, 0, 38, '2022-03-27 12:08:57', 38, '2022-03-27 12:09:02');
INSERT INTO `t_operation` VALUES (125, 7, 93, 200, '导出EXCEL', 'x-exportExcel', 1, 0, 38, '2022-03-27 12:08:57', 38, '2022-03-27 12:09:02');
COMMIT;

-- ----------------------------
-- Table structure for t_organization
-- ----------------------------
DROP TABLE IF EXISTS `t_organization`;
CREATE TABLE `t_organization` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `parent_id` int NOT NULL DEFAULT '0' COMMENT '上级组织ID',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '组织名称',
  `level` int NOT NULL DEFAULT '0' COMMENT '组织级别',
  `path` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '组织路径',
  `status` int NOT NULL DEFAULT '1' COMMENT '状态 1启用 2禁用 0删除',
  `order_no` int NOT NULL DEFAULT '0' COMMENT '序号',
  `create_uid` int NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_uid` int NOT NULL DEFAULT '0',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `parent_name` (`parent_id`,`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_organization
-- ----------------------------
BEGIN;
INSERT INTO `t_organization` VALUES (1, 0, 'x集团总部', 0, '/', 1, 1999, 1, '2018-03-21 10:25:43', 1, '2018-03-21 10:25:43');
INSERT INTO `t_organization` VALUES (2, 1, '技术部', 1, '/1/', 1, 12, 1, '2018-03-21 10:26:03', 1, '2018-03-21 10:26:03');
INSERT INTO `t_organization` VALUES (3, 1, '运营部', 1, '/1/', 1, 11, 1, '2018-03-21 10:26:25', 1, '2018-03-21 10:26:25');
INSERT INTO `t_organization` VALUES (4, 1, '客户部', 1, '/1/', 2, 13, 1, '2018-03-22 14:56:07', 1, '2018-03-22 14:56:07');
INSERT INTO `t_organization` VALUES (5, 1, '市场部门', 0, '/', 1, 14, 1, '2021-06-10 10:06:32', 1, '2021-06-10 10:06:32');
INSERT INTO `t_organization` VALUES (13, 1, '内容部', 1, '/1/', 1, 0, 1, '2021-07-19 17:26:58', 1, '2021-07-19 17:26:58');
INSERT INTO `t_organization` VALUES (14, 13, '编辑', 2, '/1/13/', 1, 0, 1, '2021-07-19 17:27:26', 1, '2021-07-19 17:27:26');
INSERT INTO `t_organization` VALUES (20, 3, '编辑', 2, '/1/3/', 1, 100, 0, '2022-04-10 17:22:29', 0, '2022-04-10 17:22:29');
COMMIT;

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `status` int NOT NULL DEFAULT '1' COMMENT '角色状态：1、启用，2、禁用',
  `create_uid` int NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_uid` int NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_role
-- ----------------------------
BEGIN;
INSERT INTO `t_role` VALUES (1, '系统管理员', 1, 1, '2018-03-21 10:31:38', 1, '2022-04-14 12:02:33');
INSERT INTO `t_role` VALUES (5, '财务', 1, 3, '2018-07-02 10:29:09', 3, '2022-04-14 12:03:00');
INSERT INTO `t_role` VALUES (6, '版权商务', 1, 3, '2018-07-02 10:29:24', 3, '2021-07-27 19:57:27');
INSERT INTO `t_role` VALUES (7, '业务', 1, 3, '2018-09-25 16:50:57', 3, '2022-04-14 12:02:53');
INSERT INTO `t_role` VALUES (8, '商业化运营', 1, 3, '2018-10-11 11:08:31', 3, '2021-11-12 12:16:09');
COMMIT;

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `role_id` int NOT NULL DEFAULT '0' COMMENT '角色ID',
  `system_id` int NOT NULL DEFAULT '0' COMMENT '系统id',
  `menu_id` int NOT NULL DEFAULT '0' COMMENT '菜单id',
  `opera_ids` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单下的可用操作',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22100 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `t_role_menu` VALUES (22090, 7, 1, 1, '');
INSERT INTO `t_role_menu` VALUES (22091, 7, 1, 5, '');
INSERT INTO `t_role_menu` VALUES (22092, 7, 1, 4, '');
INSERT INTO `t_role_menu` VALUES (22093, 7, 1, 6, '');
INSERT INTO `t_role_menu` VALUES (22096, 8, 1, 1, '');
INSERT INTO `t_role_menu` VALUES (22097, 8, 1, 5, '');
INSERT INTO `t_role_menu` VALUES (22098, 8, 1, 4, '');
INSERT INTO `t_role_menu` VALUES (22099, 8, 1, 6, '');
COMMIT;

-- ----------------------------
-- Table structure for t_subsystem
-- ----------------------------
DROP TABLE IF EXISTS `t_subsystem`;
CREATE TABLE `t_subsystem` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `domain` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '域名',
  `syskey` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'key',
  `secret` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'secret',
  `status` int NOT NULL DEFAULT '1' COMMENT '1:有效 2:禁用',
  `order_no` int NOT NULL DEFAULT '1' COMMENT '排序',
  `create_uid` int NOT NULL DEFAULT '0' COMMENT '创建人id',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `update_uid` int NOT NULL DEFAULT '0' COMMENT '更新用户id',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `domain` (`domain`) USING BTREE,
  UNIQUE KEY `syskey` (`syskey`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_subsystem
-- ----------------------------
BEGIN;
INSERT INTO `t_subsystem` VALUES (1, '登录系统', 'http://127.0.0.1:8200/', 'a8f391aa78d852f31e73189e49f12968', '6ad1411abfa8f00c5b0fedf03f9d8229', 1, 1, 1, '2018-03-21 10:11:51', 1, '2022-04-14 12:04:27');
INSERT INTO `t_subsystem` VALUES (7, 'x后台', 'http://www.baidu.com', '9dd285060a7e0b427d5cb921d33a7496', '1e51d45ed174f929af478468e5988373', 1, 7, 1, '2019-10-24 12:32:13', 1, '2022-04-14 12:04:12');
COMMIT;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登陆名',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `real_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '真实名字',
  `is_root` tinyint NOT NULL DEFAULT '0' COMMENT '是否root用户',
  `is_staff` tinyint NOT NULL DEFAULT '1' COMMENT '是否内部员工',
  `staff_no` int NOT NULL DEFAULT '0' COMMENT '员工号',
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '员工邮箱',
  `phone` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '员工手机号',
  `status` int NOT NULL DEFAULT '1' COMMENT '是否在职，1:在职，0:离职',
  `avatar` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户头像',
  `role_id` int NOT NULL DEFAULT '0' COMMENT '角色id',
  `org_id` int NOT NULL DEFAULT '0' COMMENT '所属组织',
  `create_uid` int NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_uid` int NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_name` (`name`) USING BTREE,
  UNIQUE KEY `idx_email` (`email`) USING BTREE,
  UNIQUE KEY `idx_phone` (`phone`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_user
-- ----------------------------
BEGIN;
INSERT INTO `t_user` VALUES (1, 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b', '管理员', 1, 1, 1, 'root@qq.com', '15900000000', 1, '', 0, 1, 1, '2018-03-15 12:17:29', 1, '2022-04-15 15:19:21');
INSERT INTO `t_user` VALUES (111, '111', '7c4a8d09ca3762af61e59520943dc26494f8941b', '111', 2, 1, 0, 'you@yeah.net', '18500888888', 1, '', 0, 20, 0, '2022-04-14 18:11:01', 0, '2022-04-14 22:30:00');
INSERT INTO `t_user` VALUES (112, 'test2', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test2', 2, 1, 0, 'qqq@s.com', '2131231', 1, '', 0, 16, 0, '2022-04-14 18:20:54', 0, '2022-04-14 22:30:11');
INSERT INTO `t_user` VALUES (113, 'demo1', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'demo1', 1, 2, 0, 'demo1@ss.com', '1234561', 1, '', 0, 20, 0, '2022-04-15 15:18:57', 0, '2022-04-15 15:18:57');
INSERT INTO `t_user` VALUES (114, 'demo2', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'demo2', 2, 1, 0, 'demo2@xx.com', '18590099999', 1, '', 0, 2, 0, '2022-04-15 15:55:00', 0, '2022-04-15 15:55:00');
COMMIT;

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL DEFAULT '0' COMMENT '角色id',
  `role_id` int NOT NULL DEFAULT '0' COMMENT '所属组织',
  `create_uid` int NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
BEGIN;
INSERT INTO `t_user_role` VALUES (116, 1, 17, 0, '2022-04-14 08:03:08');
INSERT INTO `t_user_role` VALUES (117, 108, 7, 0, '2022-04-14 18:15:28');
INSERT INTO `t_user_role` VALUES (118, 108, 8, 0, '2022-04-14 18:15:28');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
