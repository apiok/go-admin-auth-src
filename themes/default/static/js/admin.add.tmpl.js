
var html = (function () {/*
<script id="sidebar-level-1" type="text/x-jquery-tmpl">
  {{if $data.child}}
  <li class="nav-item has-treeview">
    <a href="${uri}" class="nav-link">
      <i class="nav-icon fas fa-tree"></i>
      <p>
        ${name}
        <i class="right fas fa-angle-left"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
      {{tmpl($data.child) '#sidebar-level-1'}}
    </ul>
  </li>
  {{else}}
  <li class="nav-item">
    <a href="${uri}" class="nav-link">
      <i class="nav-icon far fa-circle"></i>
      <p>
        ${name}
      </p>
    </a>
  </li>
  {{/if}}
</script>
*/}).toString().match(/[^]*\/\*([^]*)\*\/\}$/)[1];
