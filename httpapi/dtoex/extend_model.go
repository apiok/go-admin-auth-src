package dtoex

import "go-admin-auth/app/models"

type TMenuEx struct {
	models.TMenu

	OperaIds   string   `json:"opera_ids" form:"opera_ids"`

	ThemeUri string `json:"theme_uri" form:"theme_uri"`

	SystemName string `json:"system_name" form:"system_name"`
	ParentName string `json:"parent_name" form:"parent_name"`

	StatusDesc string `json:"status_desc" form:"status_desc"`
	IsShowDesc string `json:"is_show_desc" form:"is_show_desc"`

	CreateUsername string `json:"create_username" form:"create_username"`
	UpdateUsername string `json:"update_username" form:"update_username"`
}

type TOperationEx struct {
	models.TOperation

	SystemName string `json:"system_name" form:"system_name"`
	ParentName string `json:"parent_name" form:"parent_name"`
	MenuName   string `json:"menu_name" form:"menu_name"`

	StatusDesc string `json:"status_desc" form:"status_desc"`

	CreateUsername string `json:"create_username" form:"create_username"`
	UpdateUsername string `json:"update_username" form:"update_username"`
}

type TOrganizationEx struct {
	models.TOrganization

	ParentName string `json:"parent_name" form:"parent_name"`

	StatusDesc string `json:"status_desc" form:"status_desc"`

	CreateUsername string `json:"create_username" form:"create_username"`
	UpdateUsername string `json:"update_username" form:"update_username"`
}

type TRoleEx struct {
	models.TRole

	StatusDesc string `json:"status_desc" form:"status_desc"`

	CreateUsername string `json:"create_username" form:"create_username"`
	UpdateUsername string `json:"update_username" form:"update_username"`
}

type TRoleMenuEx struct {
	models.TRoleMenu

	RoleName   string `json:"role_name" form:"role_name"`
	SystemName string `json:"system_name" form:"system_name"`
	MenuName   string `json:"menu_name" form:"menu_name"`
}

type TSubsystemEx struct {
	models.TSubsystem

	CreateUsername string `json:"create_username" form:"create_username"`
	UpdateUsername string `json:"update_username" form:"update_username"`
}

type TUserEx struct {
	models.TUser

	OrgName    string `json:"org_name" form:"org_name"`
	IsRootDesc string `json:"is_root_desc" form:"is_root_desc"`

	CreateUsername string `json:"create_username" form:"create_username"`
	UpdateUsername string `json:"update_username" form:"update_username"`
}
