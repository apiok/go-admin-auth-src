package dtoex

import (
	"fmt"
	"go-admin-auth/app/dal"
	"go-admin-auth/app/models"
	"go-admin-auth/conf"
	"go-admin-auth/httpapi/enums"
)

type MenuQueryResEx struct {
	Total int64     `json:"total" form:"total"`
	List  []TMenuEx `json:"list" form:"list"`
}

func ExtendMenuList(resList []models.TMenu) []TMenuEx {
	exList := make([]TMenuEx, 0, len(resList))

	systemIdList := make([]int, 0)
	menuIdList := make([]int, 0)
	uidList := make([]int, 0)

	for _, v := range resList {
		systemIdList = append(systemIdList, v.SystemId)
		menuIdList = append(menuIdList, v.ParentId)
		uidList = append(uidList, v.CreateUid, v.UpdateUid)
	}

	systemMap, _ := dal.SubsystemDAO.GetMulti(systemIdList)
	menuMap, _ := dal.MenuDAO.GetMulti(menuIdList)
	userMap, _ := dal.UserDAO.GetMulti(uidList)

	for _, v := range resList {
		tActionEx := TMenuEx{TMenu: v}

		if v.Uri != "/" {
			tActionEx.ThemeUri = fmt.Sprintf("/themes/%s%s.html", conf.Theme, v.Uri)
		}

		tActionEx.SystemName = systemMap[v.SystemId].Name
		tActionEx.ParentName = menuMap[v.ParentId].Name
		tActionEx.CreateUsername = userMap[v.CreateUid].RealName
		tActionEx.UpdateUsername = userMap[v.UpdateUid].RealName

		tActionEx.StatusDesc, _ = enums.StatusMap[v.Status]
		tActionEx.IsShowDesc, _ = enums.MenuShowMap[v.IsShow]

		exList = append(exList, tActionEx)
	}
	return exList
}

type OperationQueryResEx struct {
	Total int64          `json:"total" form:"total"`
	List  []TOperationEx `json:"list" form:"list"`
}

type OrganizationQueryResEx struct {
	Total int64             `json:"total" form:"total"`
	List  []TOrganizationEx `json:"list" form:"list"`
}

type RoleQueryResEx struct {
	Total int64     `json:"total" form:"total"`
	List  []TRoleEx `json:"list" form:"list"`
}

type RoleMenuQueryResEx struct {
	Total int64         `json:"total" form:"total"`
	List  []TRoleMenuEx `json:"list" form:"list"`
}

type SubsystemQueryResEx struct {
	Total int64          `json:"total" form:"total"`
	List  []TSubsystemEx `json:"list" form:"list"`
}

type UserQueryResEx struct {
	Total int64     `json:"total" form:"total"`
	List  []TUserEx `json:"list" form:"list"`
}
