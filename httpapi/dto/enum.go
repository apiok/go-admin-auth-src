package dto

type EnumOption struct {
	Name  string      `json:"name" form:"name"`
	Value interface{} `json:"value" form:"value"`
}

type EnumIntOption struct {
	Name  string `json:"name" form:"name"`
	Value int    `json:"value" form:"value"`
}

type EnumStrOption struct {
	Name  string `json:"name" form:"name"`
	Value int    `json:"value" form:"value"`
}
