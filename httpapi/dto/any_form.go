package dto

import (
	"encoding/json"
	"go-admin-auth/app/models"
)

type AnyForm struct {
	PageForm

	RestMethod string `json:"x_rest_method" form:"x_rest_method"`

	Resource string `json:"resource" form:"resource"`
	Id       int    `json:"id" form:"id"`

	Params map[string]interface{} `json:"params" form:"params"`
}

func (anyForm *AnyForm) MenuModel() models.TMenu {
	var model models.TMenu
	paramsByte, _ := json.Marshal(anyForm.Params)
	_ = json.Unmarshal(paramsByte, &model)
	return model
}

func (anyForm *AnyForm) ToMenuQueryForm() *MenuQueryForm {
	form := new(MenuQueryForm)
	form.PageForm = anyForm.PageForm

	form.TMenu = anyForm.MenuModel()
	return form
}

func (anyForm *AnyForm) OperationModel() models.TOperation {
	var model models.TOperation
	paramsByte, _ := json.Marshal(anyForm.Params)
	_ = json.Unmarshal(paramsByte, &model)
	return model
}

func (anyForm *AnyForm) ToOperationQueryForm() *OperationQueryForm {
	form := new(OperationQueryForm)
	form.PageForm = anyForm.PageForm

	form.TOperation = anyForm.OperationModel()
	return form
}

func (anyForm *AnyForm) OrganizationModel() models.TOrganization {
	var model models.TOrganization
	paramsByte, _ := json.Marshal(anyForm.Params)
	_ = json.Unmarshal(paramsByte, &model)
	return model
}

func (anyForm *AnyForm) ToOrganizationQueryForm() *OrganizationQueryForm {
	form := new(OrganizationQueryForm)
	form.PageForm = anyForm.PageForm

	form.TOrganization = anyForm.OrganizationModel()
	return form
}

func (anyForm *AnyForm) RoleModel() models.TRole {
	var model models.TRole
	paramsByte, _ := json.Marshal(anyForm.Params)
	_ = json.Unmarshal(paramsByte, &model)
	return model
}

func (anyForm *AnyForm) ToRoleQueryForm() *RoleQueryForm {
	form := new(RoleQueryForm)
	form.PageForm = anyForm.PageForm

	form.TRole = anyForm.RoleModel()
	return form
}

func (anyForm *AnyForm) RoleMenuModel() models.TRoleMenu {
	var model models.TRoleMenu
	paramsByte, _ := json.Marshal(anyForm.Params)
	_ = json.Unmarshal(paramsByte, &model)
	return model
}

func (anyForm *AnyForm) ToRoleMenuQueryForm() *RoleMenuQueryForm {
	form := new(RoleMenuQueryForm)
	form.PageForm = anyForm.PageForm

	form.TRoleMenu = anyForm.RoleMenuModel()
	return form
}

func (anyForm *AnyForm) SubsystemModel() models.TSubsystem {
	var model models.TSubsystem
	paramsByte, _ := json.Marshal(anyForm.Params)
	_ = json.Unmarshal(paramsByte, &model)
	return model
}

func (anyForm *AnyForm) ToSubsystemQueryForm() *SubsystemQueryForm {
	form := new(SubsystemQueryForm)
	form.PageForm = anyForm.PageForm

	form.TSubsystem = anyForm.SubsystemModel()
	return form
}

func (anyForm *AnyForm) UserModel() models.TUser {
	var model models.TUser
	paramsByte, _ := json.Marshal(anyForm.Params)
	_ = json.Unmarshal(paramsByte, &model)
	return model
}

func (anyForm *AnyForm) ToUserQueryForm() *UserQueryForm {
	form := new(UserQueryForm)
	form.PageForm = anyForm.PageForm

	form.TUser = anyForm.UserModel()
	return form
}
