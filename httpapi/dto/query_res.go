package dto

import (
	"go-admin-auth/app/models"
)

type MenuQueryRes struct {
	Total int64          `json:"total" form:"total"`
	List  []models.TMenu `json:"list" form:"list"`
}

type OperationQueryRes struct {
	Total int64               `json:"total" form:"total"`
	List  []models.TOperation `json:"list" form:"list"`
}

type OrganizationQueryRes struct {
	Total int64                  `json:"total" form:"total"`
	List  []models.TOrganization `json:"list" form:"list"`
}

type RoleQueryRes struct {
	Total int64          `json:"total" form:"total"`
	List  []models.TRole `json:"list" form:"list"`
}

type RoleMenuQueryRes struct {
	Total int64              `json:"total" form:"total"`
	List  []models.TRoleMenu `json:"list" form:"list"`
}

type SubsystemQueryRes struct {
	Total int64               `json:"total" form:"total"`
	List  []models.TSubsystem `json:"list" form:"list"`
}

type UserQueryRes struct {
	Total int64          `json:"total" form:"total"`
	List  []models.TUser `json:"list" form:"list"`
}

type UserRoleQueryRes struct {
	Total int64              `json:"total" form:"total"`
	List  []models.TUserRole `json:"list" form:"list"`
}
