package dto

import "go-admin-auth/app/models"

type RoleMenuPermDTO struct {
	*models.TRole

	MenuIdList       []int    `json:"menu_id_list" form:"menu_id_list"`
	OperaIdList      []int    `json:"opera_id_list" form:"opera_id_list"`
	MenuOperaCombine []string `json:"menu_opera_combine" form:"menu_opera_combine"`
}

type MenuOperaPermDTO struct {
	models.TMenu

	OperationList []*models.TOperation `json:"operation_list" form:"operation_list"`
}

// 保存角色权限
type RoleMenuPermForm struct {
	SystemId   int      `json:"system_id" form:"system_id"`
	RoleId     int      `json:"role_id" form:"role_id"`
	MenuIdList []int    `json:"menu_id_list[]" form:"menu_id_list[]"`
	OperaList  []string `json:"opera_list[]" form:"opera_list[]"`
}

type RoleMenuPermQueryForm struct {
	SystemId int `json:"system_id" form:"system_id"`
	RoleId   int `json:"role_id" form:"role_id"`
}

type systemPermInfo struct {
	MenuIdList      []int `json:"menu_id_list[]" form:"menu_id_list[]"`
	OperationIdList []int `json:"operation_id_list" form:"operation_id_list"`
}

type UserRoleDTO struct {
	UserId int `json:"user_id" form:"user_id"`

	RoleIdList []int `json:"role_id_list[]" form:"role_id_list[]"`
	OperaUid int `json:"opera_uid" form:"opera_uid"`
}
