package dto

import (
	"go-admin-auth/app/models"
)

type MultiForm struct {
	Ids string `json:"ids" form:"ids"`
}

type MenuQueryForm struct {
	models.TMenu
	PageForm
	Ids    string `json:"ids" form:"ids"`
	IdList []int  `json:"id_list" form:"id_list"`
}

type OperationQueryForm struct {
	models.TOperation
	PageForm

	SearchMenuId int `json:"search_menu_id" form:"search_menu_id"`
}

type OrganizationQueryForm struct {
	models.TOrganization
	PageForm

	ParentPath string `json:"parent_path,default=/" form:"parent_path,default=/"`
}

type RoleQueryForm struct {
	models.TRole
	PageForm
}

type RoleMenuQueryForm struct {
	models.TRoleMenu
	PageForm

	RoleIdList []int
}

type SubsystemQueryForm struct {
	models.TSubsystem
	PageForm
}

type UserQueryForm struct {
	models.TUser
	PageForm

	OrgIdList []int
}

type UserRoleQueryForm struct {
	models.TUserRole
	PageForm
}


