package enums

import "go-admin-auth/app/lib"

/** 普通，通用 表status **/
type status struct {
	Valid   int `v:"1" d:"启用"`
	Invalid int `v:"2" d:"禁用"`
}

type yesOrNo struct {
	Yes int `v:"1" d:"是"`
	No  int `v:"2" d:"否"`
}

var (
	statusState, statusMap = lib.InitState(status{})
	StatusType             = statusState.(status)
	StatusMap              = statusMap

	yesOrNoState, yesOrNoMap = lib.InitState(yesOrNo{})
	YesOrNoType              = yesOrNoState.(yesOrNo)
	YesOrNoMap               = yesOrNoMap
)
