package enums

import "go-admin-auth/app/lib"

type isRoot struct {
	No  int `v:"0" d:"否"`
	Yes int `v:"1" d:"是"`
}

var (
	isRootState, isRootMap = lib.InitState(isRoot{})
	IsRootType             = isRootState.(isRoot)
	IsRootMap              = isRootMap
)
