package enums

import "go-admin-auth/app/lib"

type menuShow struct {
	Valid   int `v:"1" d:"正常"`
	Invalid int `v:"2" d:"隐藏"`
}

var (
	menuShowState, menuShowMap = lib.InitState(menuShow{})
	MenuShowType               = menuShowState.(menuShow)
	MenuShowMap                = menuShowMap
)
