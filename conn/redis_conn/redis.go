package redis_conn

import (
	"context"
	"github.com/go-redis/redis/v8"
	"os"
	"strconv"
	"sync"
)

// document
//https://redis.uptrace.dev/guide/server.html#connecting-to-redis-server

var rdb *redis.Client
var once sync.Once

var BGCtx context.Context

func initRedisDB() {
	BGCtx = context.Background()

	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")
	password := os.Getenv("REDIS_PASSWORD")
	db := os.Getenv("REDIS_DEFAULT_DB")
	intDb, _ := strconv.Atoi(db)

	rdb = redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Password: password, // no password set
		DB:       intDb,    // use default DB
	})
}

func GetRds() *redis.Client {
	once.Do(initRedisDB)
	return rdb
}

