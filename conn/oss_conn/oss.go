package oss_conn

import (
	"errors"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"os"
	"sync"
)

var client *oss.Client
var once sync.Once

func ossClient() {
	Endpoint := os.Getenv("OSS_END_POINT")
	AccessKeyId := os.Getenv("OSS_ACCESS_KEY_ID")
	AccessKeySecret := os.Getenv("OSS_ACCESS_KEY_SECRET")

	ossClient, err := oss.New(Endpoint, AccessKeyId, AccessKeySecret)
	if err != nil {
		panic(err)
	}

	client = ossClient
	// lsRes, err := client.ListBuckets()
	// if err != nil {
	// 	// HandleError(err)
	// }

	// for _, bucket := range lsRes.Buckets {
	// 	fmt.Println("Buckets:", bucket.Name)
	// }
}

func DefaultBucket() (*oss.Bucket, error) {
	once.Do(ossClient)

	if client == nil {
		return nil, errors.New("client invalid")
	}

	bucketName := os.Getenv("OSS_BUCKET")
	return client.Bucket(bucketName)
}
