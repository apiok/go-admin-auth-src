package models

type TMenu struct {
	Id         int      `json:"id" form:"id"`
	SystemId   int      `json:"system_id" form:"system_id"`
	ParentId   int      `json:"parent_id" form:"parent_id"`
	Name       string   `json:"name" form:"name"`
	Level      int      `json:"level" form:"level"`
	Path       string   `json:"path" form:"path"`
	Uri        string   `json:"uri" form:"uri"`
	TargetTab  string   `json:"target_tab" form:"target_tab"`
	Status     int      `json:"status" form:"status"`
	OrderNo    int      `json:"order_no" form:"order_no"`
	IsShow     int      `json:"is_show" form:"is_show"`
	CreateUid  int      `json:"create_uid" form:"create_uid"`
	CreateTime DateTime `json:"create_time" form:"create_time" gorm:"autoCreateTime"`
	UpdateUid  int      `json:"update_uid" form:"update_uid"`
	UpdateTime DateTime `json:"update_time" form:"update_time" gorm:"autoCreateTime"`
}
