package models

type TOperation struct {
	Id         int      `json:"id" form:"id"`
	SystemId   int      `json:"system_id" form:"system_id"`
	ParentId   int      `json:"parent_id" form:"parent_id"`
	MenuId     int      `json:"menu_id" form:"menu_id"`
	Name       string   `json:"name" form:"name"`
	Code       string   `json:"code" form:"code"`
	Status     int      `json:"status" form:"status"`
	OrderNo    int      `json:"order_no" form:"order_no"`
	CreateUid  int      `json:"create_uid" form:"create_uid"`
	CreateTime DateTime `json:"create_time" form:"create_time" gorm:"autoCreateTime"`
	UpdateUid  int      `json:"update_uid" form:"update_uid"`
	UpdateTime DateTime `json:"update_time" form:"update_time" gorm:"autoCreateTime"`
}
