package models

type TUserRole struct {
	Id     int `json:"id" form:"id"`
	UserId int `json:"user_id" form:"user_id"`
	RoleId int `json:"role_id" form:"role_id"`
	CreateUid  int      `json:"create_uid" form:"create_uid"`
	CreateTime DateTime `json:"create_time" form:"create_time" gorm:"autoCreateTime"`
}
