package models

type TUser struct {
	Id         int      `json:"id" form:"id"`
	Name       string   `json:"name" form:"name"`
	Password   string   `json:"-" form:"password"`
	RealName   string   `json:"real_name" form:"real_name"`
	IsRoot     int      `json:"is_root" form:"is_root"`
	IsStaff    int      `json:"is_staff" form:"is_staff"`
	StaffNo    int      `json:"staff_no" form:"staff_no"`
	Email      string   `json:"email" form:"email"`
	Phone      string   `json:"phone" form:"phone"`
	Status     int      `json:"status" form:"status"`
	Avatar     string   `json:"avatar" form:"avatar"`
	RoleId     int      `json:"role_id" form:"role_id"`
	OrgId      int      `json:"org_id" form:"org_id"`
	CreateUid  int      `json:"create_uid" form:"create_uid"`
	CreateTime DateTime `json:"create_time" form:"create_time" gorm:"autoCreateTime"`
	UpdateUid  int      `json:"update_uid" form:"update_uid"`
	UpdateTime DateTime `json:"update_time" form:"update_time" gorm:"autoCreateTime"`
}
