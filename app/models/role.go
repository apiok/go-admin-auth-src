package models

type TRole struct {
	Id         int      `json:"id" form:"id"`
	Name       string   `json:"name" form:"name"`
	Status     int      `json:"status" form:"status"`
	CreateUid  int      `json:"create_uid" form:"create_uid"`
	CreateTime DateTime `json:"create_time" form:"create_time" gorm:"autoCreateTime"`
	UpdateUid  int      `json:"update_uid" form:"update_uid"`
	UpdateTime DateTime `json:"update_time" form:"update_time" gorm:"autoCreateTime"`
}
