package models

type TSubsystem struct {
	Id         int      `json:"id" form:"id"`
	Name       string   `json:"name" form:"name"`
	Domain     string   `json:"domain" form:"domain"`
	Syskey     string   `json:"syskey" form:"syskey"`
	Secret     string   `json:"secret" form:"secret"`
	Status     int      `json:"status" form:"status"`
	OrderNo    int      `json:"order_no" form:"order_no"`
	CreateUid  int      `json:"create_uid" form:"create_uid"`
	CreateTime DateTime `json:"create_time" form:"create_time" gorm:"autoCreateTime"`
	UpdateUid  int      `json:"update_uid" form:"update_uid"`
	UpdateTime DateTime `json:"update_time" form:"update_time" gorm:"autoCreateTime"`
}
