package models

type TRoleMenu struct {
	Id       int    `json:"id" form:"id"`
	RoleId   int    `json:"role_id" form:"role_id"`
	SystemId int    `json:"system_id" form:"system_id"`
	MenuId   int    `json:"menu_id" form:"menu_id"`
	OperaIds string `json:"opera_ids" form:"opera_ids"`
}
