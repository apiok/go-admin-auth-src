package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/models"
	"go-admin-auth/app/service"
	"go-admin-auth/gin_helper"
	"go-admin-auth/httpapi/dto"
)

type organizationHandler struct {
}

var (
	OrganizationHandler = new(organizationHandler)
)

func (h organizationHandler) Query(c *gin.Context) {
	form := new(dto.OrganizationQueryForm)
	if err := c.ShouldBind(&form); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	form.OrderBy = append(form.OrderBy, "id desc")

	ret, err := service.OrganizationSrv.Query(form)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, ret)
}

func (h organizationHandler) Get(c *gin.Context) {
	id, _ := gin_helper.ParamInt(c, "id")

	info, err := service.OrganizationSrv.Get(id)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, info)
}

func (h organizationHandler) Add(c *gin.Context) {
	info := new(models.TOrganization)
	if err := c.ShouldBind(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	if err := service.OrganizationSrv.Insert(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, info)
}

func (h organizationHandler) Edit(c *gin.Context) {
	info := new(models.TOrganization)
	id, _ := gin_helper.ParamInt(c, "id")
	info.Id = id
	if err := c.ShouldBind(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	if err := service.OrganizationSrv.Update(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, info)
}

func (h organizationHandler) Delete(c *gin.Context) {
	id, _ := gin_helper.ParamInt(c, "id")
	if err := service.OrganizationSrv.RealDelete(id); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, "")
}
