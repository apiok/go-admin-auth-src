package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/service/org"
	"go-admin-auth/gin_helper"
	"strconv"
)

type orgPermHandler struct {
}

var (
	OrgPermHandler = new(orgPermHandler)
)

func (h *orgPermHandler) GetSubUserIdList(c *gin.Context) {
	userIdStr := c.Query("user_id")
	userId, _ := strconv.Atoi(userIdStr)

	userIdList, err := org.OrgCtl.GetSubUserIdListByUserId(userId)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, userIdList)
}

func (h *orgPermHandler) GetSubUserList(c *gin.Context) {
	userIdStr := c.Query("user_id")
	userId, _ := strconv.Atoi(userIdStr)

	userIdList, err := org.OrgCtl.GetSubUserListByUserId(userId)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, userIdList)
}
