package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/service"
	"go-admin-auth/gin_helper"
)

type enumHandler struct {
}

var (
	EnumHandler = new(enumHandler)
)

func (h enumHandler) GetOptions(c *gin.Context) {
	name := c.Query("name")
	var ret interface{}
	switch name {
	case "status":
		ret = service.EnumSrv.GetCommonStatusOptions()
	case "yes_or_no":
		ret = service.EnumSrv.GetCommonYesOrNoOptions()
	case "org_status":
		ret = service.EnumSrv.GetOrgStatusOptions()
	case "user_is_root":
		ret = service.EnumSrv.GetUserIsRootOptions()
	case "user_is_staff":
		ret = service.EnumSrv.GetUserIsStuffOptions()
	case "user_status":
		ret = service.EnumSrv.GetOrgStatusOptions()

	}
	gin_helper.JsonOk(c, ret)
}
