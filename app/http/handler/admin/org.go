package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/service"
	"go-admin-auth/gin_helper"
	"strconv"
)

type orgToolHandler struct {
}

var (
	OrgToolHandler = new(orgToolHandler)
)

func (h *orgToolHandler) GetSelectOptions(c *gin.Context) {
	parentIdStr := c.Request.FormValue("parent_id")
	parentId, _ := strconv.Atoi(parentIdStr)

	ret, _ := service.OrganizationSrv.GetOrgSelectOptions(parentId)
	gin_helper.JsonOk(c, ret)
}
