package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/service"
	"go-admin-auth/app/service/rbac"
	"go-admin-auth/gin_helper"
	"go-admin-auth/httpapi/dto"
	"strconv"
)

type menuToolHandler struct {
}

var (
	MenuToolHandler = new(menuToolHandler)
)

func (h menuToolHandler) GetSideBarTree(c *gin.Context) {
	systemIdStr := c.Request.FormValue("system_id")
	systemId, _ := strconv.Atoi(systemIdStr)

	form := new(dto.MenuQueryForm)
	form.SystemId = systemId
	form.Page = 1
	form.Psize = 10000

	form.OrderBy = append(form.OrderBy, "system_id asc")
	form.OrderBy = append(form.OrderBy, "order_no asc")

	userId := gin_helper.GetCurrentUserId(c)
	if userId != 0 {
		menuIdList, _ := rbac.RBACMenuSrv.GetMenuIdListByUserId(userId)
		form.IdList = menuIdList
	}

	ret, _ := service.MenuSrv.GetMenuTree(form)
	//c.Negotiate(http.StatusOK, gin.Negotiate{
	//	Offered: []string{gin.MIMEXML},
	//	Data:    gin.H{"code": define.SUCCESS, "msg": define.StatusText(define.SUCCESS), "data": ret},
	//})
	gin_helper.JsonOk(c, ret)
}

func (h menuToolHandler) GetSelectOptions(c *gin.Context) {
	systemIdStr := c.Request.FormValue("system_id")
	systemId, _ := strconv.Atoi(systemIdStr)

	parentIdStr := c.Request.FormValue("parent_id")
	parentId, _ := strconv.Atoi(parentIdStr)

	ret, _ := service.MenuSrv.GetMenuSelectOptions(systemId, parentId)
	gin_helper.JsonOk(c, ret)
}
