package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/models"
	"go-admin-auth/app/service"
	"go-admin-auth/gin_helper"
	"go-admin-auth/httpapi/dto"
)

type roleMenuHandler struct {
}

var (
	RoleMenuHandler = new(roleMenuHandler)
)

func (h roleMenuHandler) Query(c *gin.Context) {
	form := new(dto.RoleMenuQueryForm)
	if err := c.ShouldBind(&form); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	ret, err := service.RoleMenuSrv.Query(form)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, ret)
}

func (h roleMenuHandler) Get(c *gin.Context) {
	id, _ := gin_helper.ParamInt(c, "id")

	info, err := service.RoleMenuSrv.Get(id)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, info)
}

func (h roleMenuHandler) Add(c *gin.Context) {
	info := new(models.TRoleMenu)
	if err := c.ShouldBind(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	if err := service.RoleMenuSrv.Insert(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, info)
}

func (h roleMenuHandler) Edit(c *gin.Context) {
	info := new(models.TRoleMenu)
	id, _ := gin_helper.ParamInt(c, "id")
	info.Id = id
	if err := c.ShouldBind(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	if err := service.RoleMenuSrv.Update(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, info)
}

func (h roleMenuHandler) Delete(c *gin.Context) {
	id, _ := gin_helper.ParamInt(c, "id")
	if err := service.RoleMenuSrv.RealDelete(id); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, "")
}
