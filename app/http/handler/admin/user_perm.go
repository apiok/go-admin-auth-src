package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/service/rbac"
	"go-admin-auth/gin_helper"
	"strconv"
)

type userPermHandler struct {
}

var (
	UserPermHandler = new(userPermHandler)
)

func (h *userPermHandler) GetUserOperationIdList(c *gin.Context) {
	userIdStr := c.Query("user_id")
	userId, _ := strconv.Atoi(userIdStr)

	systemIdStr := c.Query("system_id")
	systemId, _ := strconv.Atoi(systemIdStr)

	info, err := rbac.RBACOperaSrv.GetUserOperationIdList(userId, systemId)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, info)
}

func (h *userPermHandler) GetUserOperationList(c *gin.Context) {

}
