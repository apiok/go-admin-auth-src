package admin

import (
	"errors"
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/service"
	"go-admin-auth/gin_helper"
	"go-admin-auth/gin_helper/define"
	"go-admin-auth/httpapi/dto"
	"strconv"
)

type anyResourceHandler struct {
}

var (
	AnyResourceHandler = new(anyResourceHandler)
)

func (h *anyResourceHandler) Any(c *gin.Context) {
	var err error

	form := new(dto.AnyForm)
	err = c.ShouldBind(form)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	form.Resource = c.Request.FormValue("resource")
	form.Id, _ = strconv.Atoi(c.Request.FormValue("id"))

	if form.RestMethod == "" {
		form.RestMethod = c.Request.FormValue(define.RestMethodKey)
	}

	form.Params, _ = gin_helper.PureRestParams(c)

	var res interface{}
	switch form.RestMethod {
	//case "query":
	//	res, err = h.AnyQuery(form)
	//case "get":
	//	res, err = h.AnyGet(form)
	//case "create":
	//	res, err = h.AnyCreate(form)
	//case "update":
	//	res, err = h.AnyUpdate(form)
	case "delete":
		err = h.AnyDelete(form)
	default:
		err = errors.New("method not support")
	}

	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, res)
}

func (h *anyResourceHandler) AnyQuery(form *dto.AnyForm) (interface{}, error) {

	switch form.Resource {
	case "menu":
		return service.MenuSrv.Query(form.ToMenuQueryForm())
	case "operation":
		return service.OperationSrv.Query(form.ToOperationQueryForm())
	case "organization":
		return service.OrganizationSrv.Query(form.ToOrganizationQueryForm())
	case "role":
		return service.RoleSrv.Query(form.ToRoleQueryForm())
	case "role_menu":
		return service.RoleMenuSrv.Query(form.ToRoleMenuQueryForm())
	case "subsystem":
		return service.SubsystemSrv.Query(form.ToSubsystemQueryForm())
	case "user":
		return service.UserSrv.Query(form.ToUserQueryForm())
	}

	return nil, errors.New("resource not support")
}

func (h *anyResourceHandler) AnyGet(form *dto.AnyForm) (interface{}, error) {
	switch form.Resource {
	case "menu":
		return service.MenuSrv.Get(form.Id)
	case "operation":
		return service.OperationSrv.Get(form.Id)
	case "organization":
		return service.OrganizationSrv.Get(form.Id)
	case "role":
		return service.RoleSrv.Get(form.Id)
	case "role_menu":
		return service.RoleMenuSrv.Get(form.Id)
	case "subsystem":
		return service.SubsystemSrv.Get(form.Id)
	case "user":
		return service.UserSrv.Get(form.Id)
	}

	return nil, errors.New("resource not support")
}

func (h *anyResourceHandler) AnyCreate(form *dto.AnyForm) (interface{}, error) {
	switch form.Resource {
	case "menu":
		model := form.MenuModel()
		err := service.MenuSrv.Insert(&model)
		return model, err
	case "operation":
		model := form.OperationModel()
		err := service.OperationSrv.Insert(&model)
		return model, err
	case "organization":
		model := form.OrganizationModel()
		err := service.OrganizationSrv.Insert(&model)
		return model, err
	case "role":
		model := form.RoleModel()
		err := service.RoleSrv.Insert(&model)
		return model, err
	case "role_menu":
		model := form.RoleMenuModel()
		err := service.RoleMenuSrv.Insert(&model)
		return model, err
	case "subsystem":
		model := form.SubsystemModel()
		err := service.SubsystemSrv.Insert(&model)
		return model, err
	case "user":
		model := form.UserModel()
		err := service.UserSrv.Insert(&model)
		return model, err
	}
	return nil, errors.New("resource not support")
}

func (h *anyResourceHandler) AnyUpdate(form *dto.AnyForm) (interface{}, error) {
	switch form.Resource {
	case "menu":
		model := form.MenuModel()
		err := service.MenuSrv.Update(&model)
		return model, err
	case "operation":
		model := form.OperationModel()
		err := service.OperationSrv.Update(&model)
		return model, err
	case "organization":
		model := form.OrganizationModel()
		err := service.OrganizationSrv.Update(&model)
		return model, err
	case "role":
		model := form.RoleModel()
		err := service.RoleSrv.Update(&model)
		return model, err
	case "role_menu":
		model := form.RoleMenuModel()
		err := service.RoleMenuSrv.Update(&model)
		return model, err
	case "subsystem":
		model := form.SubsystemModel()
		err := service.SubsystemSrv.Update(&model)
		return model, err
	case "user":
		model := form.UserModel()
		err := service.UserSrv.Update(&model)
		return model, err
	}
	return nil, errors.New("resource not support")
}

func (h *anyResourceHandler) AnyDelete(form *dto.AnyForm) error {
	switch form.Resource {
	case "menu":
		return service.MenuSrv.Delete(form.Id)
	case "operation":
		return service.OperationSrv.Delete(form.Id)
	case "organization":
		return service.OrganizationSrv.Delete(form.Id)
	case "role":
		return service.RoleSrv.Delete(form.Id)
	case "role_menu":
		return service.RoleMenuSrv.Delete(form.Id)
	case "subsystem":
		return service.SubsystemSrv.Delete(form.Id)
	case "user":
		return service.UserSrv.Delete(form.Id)
	}

	return errors.New("resource not support")
}
