package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/service"
	"go-admin-auth/gin_helper"
	"go-admin-auth/httpapi/dto"
	"strconv"
)

type roleMenuPermHandler struct {
}

var (
	RoleMenuPermHandler = new(roleMenuPermHandler)
)

func (h *roleMenuPermHandler) Query(c *gin.Context) {
	form := new(dto.RoleMenuPermQueryForm)
	if err := c.ShouldBind(&form); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	info, err := service.RoleMenuPermSrv.Query(form)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, info)
}

func (h *roleMenuPermHandler) Get(c *gin.Context) {
	roleIdStr := c.Query("role_id")
	roleId, _ := strconv.Atoi(roleIdStr)

	systemIdStr := c.Query("system_id")
	systemId, _ := strconv.Atoi(systemIdStr)

	info, err := service.RoleMenuPermSrv.Get(roleId, systemId)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, info)
}

func (h *roleMenuPermHandler) Edit(c *gin.Context) {
	info := new(dto.RoleMenuPermForm)
	if err := c.ShouldBind(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	err := service.RoleMenuPermSrv.Update(info)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, info)
}
