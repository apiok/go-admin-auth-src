package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/models"
	"go-admin-auth/app/service"
	"go-admin-auth/gin_helper"
	"go-admin-auth/httpapi/dto"
)

type subsystemHandler struct {
}

var (
	SubsystemHandler = new(subsystemHandler)
)

func (h subsystemHandler) Query(c *gin.Context) {
	form := new(dto.SubsystemQueryForm)
	if err := c.ShouldBind(&form); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	form.OrderBy = append(form.OrderBy, "order_no asc")

	ret, err := service.SubsystemSrv.Query(form)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, ret)
}

func (h subsystemHandler) Get(c *gin.Context) {
	id, _ := gin_helper.ParamInt(c, "id")

	info, err := service.SubsystemSrv.Get(id)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, info)
}

func (h subsystemHandler) Add(c *gin.Context) {
	info := new(models.TSubsystem)
	if err := c.ShouldBind(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	if err := service.SubsystemSrv.Insert(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, info)
}

func (h subsystemHandler) Edit(c *gin.Context) {
	info := new(models.TSubsystem)
	id, _ := gin_helper.ParamInt(c, "id")
	info.Id = id
	if err := c.ShouldBind(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	if err := service.SubsystemSrv.Update(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, info)
}

func (h subsystemHandler) Delete(c *gin.Context) {
	id, _ := gin_helper.ParamInt(c, "id")
	if err := service.SubsystemSrv.RealDelete(id); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, "")
}
