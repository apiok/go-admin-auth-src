package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/service"
	"go-admin-auth/gin_helper"
	"go-admin-auth/httpapi/dto"
)

type userRoleHandler struct {
}

var (
	UserRoleHandler = new(userRoleHandler)
)

func (h userRoleHandler) Query(c *gin.Context) {
	form := new(dto.UserRoleQueryForm)
	if err := c.ShouldBind(&form); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	ret, err := service.UserRoleSrv.Query(form)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, ret)
}

func (h userRoleHandler) Get(c *gin.Context) {
	id, _ := gin_helper.ParamInt(c, "id")

	info, err := service.UserRoleSrv.Get(id)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, info)
}

func (h userRoleHandler) Add(c *gin.Context) {
	info := new(dto.UserRoleDTO)
	if err := c.ShouldBind(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	if err := service.UserRoleSrv.Edit(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, info)
}
