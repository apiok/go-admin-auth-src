package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/lib"
	"go-admin-auth/app/models"
	"go-admin-auth/app/service"
	"go-admin-auth/gin_helper"
	"go-admin-auth/httpapi/dto"
	"strings"
)

type userHandler struct {
}

var (
	UserHandler = new(userHandler)
)

func (h userHandler) Query(c *gin.Context) {
	form := new(dto.UserQueryForm)
	if err := c.ShouldBind(&form); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	form.OrderBy = append(form.OrderBy, "id desc")

	ret, err := service.UserSrv.Query(form)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, ret)
}

func (h userHandler) Get(c *gin.Context) {
	id, _ := gin_helper.ParamInt(c, "id")

	info, err := service.UserSrv.Get(id)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, info)
}

func (h userHandler) GetMulti(c *gin.Context) {
	form := new(dto.MultiForm)
	if err := c.ShouldBind(&form); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	idStrList := strings.Split(form.Ids, ",")
	idList, _ := lib.StringSliceToIntSlice(idStrList)

	info, err := service.UserSrv.GetMulti(idList)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, info)
}

func (h userHandler) Add(c *gin.Context) {
	info := new(models.TUser)
	if err := c.ShouldBind(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	if err := service.UserSrv.Insert(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, info)
}

func (h userHandler) Edit(c *gin.Context) {
	info := new(models.TUser)
	id, _ := gin_helper.ParamInt(c, "id")
	info.Id = id
	if err := c.ShouldBind(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	if err := service.UserSrv.Update(info); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, info)
}

func (h userHandler) Delete(c *gin.Context) {
	id, _ := gin_helper.ParamInt(c, "id")
	if err := service.UserSrv.RealDelete(id); err != nil {
		gin_helper.JsonErr(c, err)
		return
	}

	gin_helper.JsonOk(c, "")
}
