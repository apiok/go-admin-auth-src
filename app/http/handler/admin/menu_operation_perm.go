package admin

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/service"
	"go-admin-auth/gin_helper"
	"strconv"
)

type menuOperaPermHandler struct {
}

var (
	MenuOperaPermHandler = new(menuOperaPermHandler)
)

func (h *menuOperaPermHandler) Get(c *gin.Context) {
	systemIdStr := c.Query("system_id")
	systemId, _ := strconv.Atoi(systemIdStr)

	info, err := service.MenuOperaPermSrv.Get(systemId)
	if err != nil {
		gin_helper.JsonErr(c, err)
		return
	}
	gin_helper.JsonOk(c, info)
}
