package middleware

import (
	"errors"
	"fmt"
	"go-admin-auth/conf"
	"go-admin-auth/gin_helper/define"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

//InnerToken 内容服务接口认证
func InnerToken(c *gin.Context) {
	err := CheckInnerToken(c)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"code": 1, "msg": err.Error(), "data": ""})
		c.Abort()
	}
	c.Next()
}

//CheckInnerToken 检查内部服务接口的token
func CheckInnerToken(c *gin.Context) error {
	tokenString := c.GetHeader(define.InnerTokenKey)
	if tokenString == "" {
		tokenString, _ = c.Cookie(define.InnerTokenKey)
	}
	if tokenString == "" {
		return errors.New("缺失认证token")
	}

	tokenString = strings.TrimPrefix(tokenString, "Bearer ")
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		hmacSampleSecret := conf.GetEnvDft(define.InnerJwtSecretKey, "inner-jwt-secret")
		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return []byte(hmacSampleSecret), nil
	})

	if nil != err {
		return errors.New("inner-token 签名认证失败")
	}

	if !token.Valid {
		return errors.New("inner-token 无效")
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return errors.New("inner-token, claims无效")
	}
	c.Set(define.InnerSystemId, claims["system_id"])

	return nil
}
