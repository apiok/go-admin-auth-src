package lib

import (
	"reflect"
	"strconv"
)

/**
用户初始化各种状态集合
*/
func InitState(obj interface{}) (interface{}, map[int]string) {
	var StatusMap = make(map[int]string)
	rValue := reflect.ValueOf(obj)
	rType := reflect.TypeOf(obj)
	isPtr := rType.Kind() == reflect.Ptr
	if isPtr {
		rValue = rValue.Elem()
		rType = rType.Elem()
	} else {
		rValue = reflect.New(reflect.TypeOf(obj)).Elem()
	}
	numFiled := rType.NumField()
	for i := 0; i < numFiled; i++ {
		field := rType.Field(i)
		statusStr := field.Tag.Get("v")
		desc := field.Tag.Get("d")
		if desc == "" {
			panic("every filed should have status and desc")
		}
		status, _ := strconv.Atoi(statusStr)
		StatusMap[status] = desc

		rValue.Field(i).SetInt(int64(status))
	}

	return rValue.Interface(), StatusMap
}

/**
用户初始化各种状态集合
*/
func InitStrState(obj interface{}) (interface{}, map[string]string) {
	var StatusMap = make(map[string]string)
	rValue := reflect.ValueOf(obj)
	rType := reflect.TypeOf(obj)
	isPtr := rType.Kind() == reflect.Ptr
	if isPtr {
		rValue = rValue.Elem()
		rType = rType.Elem()
	} else {
		rValue = reflect.New(reflect.TypeOf(obj)).Elem()
	}
	numFiled := rType.NumField()
	for i := 0; i < numFiled; i++ {
		field := rType.Field(i)
		statusStr := field.Tag.Get("v")
		desc := field.Tag.Get("d")
		if desc == "" {
			panic("every filed should have status and desc")
		}
		StatusMap[statusStr] = desc

		rValue.Field(i).SetString(statusStr)
	}

	return rValue.Interface(), StatusMap
}
