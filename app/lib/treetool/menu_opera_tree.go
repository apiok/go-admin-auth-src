package treetool

import (
	"go-admin-auth/app/models"
	"go-admin-auth/httpapi/dto"
)

type MenuOperaTreeNode struct {
	dto.MenuOperaPermDTO
	Child []*MenuOperaTreeNode `json:"child"`
}

type menuOperaTree struct {
	MenuList []*dto.MenuOperaPermDTO
	Tree     *MenuOperaTreeNode
}

func initMenuOperaTree(tr *MenuOperaTreeNode, menuList []*dto.MenuOperaPermDTO) {
	// 把菜单中等于tree.id的都取出来
	for _, v := range menuList {
		if v.ParentId == tr.Id {
			subTreeNode := &MenuOperaTreeNode{MenuOperaPermDTO: *v}
			tr.Child = append(tr.Child, subTreeNode)
		}
	}

	for _, subTree := range tr.Child {
		initMenuOperaTree(subTree, menuList)
	}
}

func NewMenuOperaTree(menuList []*dto.MenuOperaPermDTO, treeRootId int) *menuOperaTree {
	tTree := &MenuOperaTreeNode{
	}
	tTree.Id = treeRootId
	tTree.OperationList = make([]*models.TOperation, 0)

	initMenuOperaTree(tTree, menuList)

	mTree := &menuOperaTree{MenuList: menuList, Tree: tTree}
	return mTree
}

func (m *menuOperaTree) GetTree() *MenuOperaTreeNode {
	return m.Tree
}
