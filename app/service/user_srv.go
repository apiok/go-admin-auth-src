package service

import (
	"go-admin-auth/app/dal"
	"go-admin-auth/app/lib"
	"go-admin-auth/app/models"
	"go-admin-auth/httpapi/dto"
	"go-admin-auth/httpapi/dtoex"
)

type userSrv struct{}

var (
	UserSrv = &userSrv{}
)

func (srv *userSrv) Query(f *dto.UserQueryForm) (*dtoex.UserQueryResEx, error) {
	res, err := dal.UserDAO.Query(f)
	if err != nil {
		return nil, err
	}

	return (*dtoex.UserQueryRes)(res).Extend(), nil
}

func (srv *userSrv) Get(id int) (*models.TUser, error) {
	return dal.UserDAO.Get(id)
}

func (srv *userSrv) GetListByOrgIdList(orgIdList  []int) ([]models.TUser, error) {
	f := new (dto.UserQueryForm)
	f.Page = 1
	f.Psize = 10000
	f.OrgIdList = orgIdList

	res, err := dal.UserDAO.Query(f)
	if err != nil{
		return nil, err
	}
	return res.List, nil
}

func (srv *userSrv) GetMulti(idList []int) (map[int]models.TUser, error) {
	return dal.UserDAO.GetMulti(idList)
}

func (srv *userSrv) Insert(m *models.TUser) error {
	m.Password = lib.EncryptWord(m.Password, nil)
	return dal.UserDAO.Insert(m)
}

func (srv *userSrv) Update(m *models.TUser) error {
	return dal.UserDAO.Update(m)
}

func (srv *userSrv) Delete(id int) error {
	return dal.UserDAO.Delete(id)
}

func (srv *userSrv) RealDelete(id int) error {
	return dal.UserDAO.RealDelete(id)
}
