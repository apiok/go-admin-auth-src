package service

import (
	"go-admin-auth/app/dal"
	"go-admin-auth/app/models"
	"go-admin-auth/httpapi/dto"
)

type userRoleSrv struct{}

var (
	UserRoleSrv = &userRoleSrv{}
)

func (srv *userRoleSrv) Query(f *dto.UserRoleQueryForm) (*dto.UserRoleQueryRes, error) {
	res, err := dal.UserRoleDAO.Query(f)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (srv *userRoleSrv) GetList(f *dto.UserRoleQueryForm) ([]models.TUserRole, error) {
	return dal.UserRoleDAO.GetList(f)
}

func (srv *userRoleSrv) Get(id int) (*models.TUserRole, error) {
	return dal.UserRoleDAO.Get(id)
}


func (srv *userRoleSrv) GetMulti(idList []int) (map[int]models.TUserRole, error) {
	return dal.UserRoleDAO.GetMulti(idList)
}

func (srv *userRoleSrv) Edit(m *dto.UserRoleDTO) error {
	rows := make([]*models.TUserRole, 0)

	for _, v := range m.RoleIdList{
		row := new(models.TUserRole)
		row.UserId = m.UserId
		row.RoleId = v
		row.CreateUid = m.OperaUid
		rows = append(rows, row)
	}

	_ = dal.UserRoleDAO.RealDeleteByUserId(m.UserId)
	err := dal.UserRoleDAO.BatchInsert(rows)
	return err
}

func (srv *userRoleSrv) RealDelete(id int) error {
	return dal.UserRoleDAO.RealDelete(id)
}
