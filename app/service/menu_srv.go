package service

import (
	"go-admin-auth/app/dal"
	"go-admin-auth/app/lib/treetool"
	"go-admin-auth/app/models"
	"go-admin-auth/httpapi/dto"
	"go-admin-auth/httpapi/dtoex"
)

type menuSrv struct{}

var (
	MenuSrv = &menuSrv{}
)

func (srv *menuSrv) Query(f *dto.MenuQueryForm) (*dtoex.MenuQueryResEx, error) {
	res, err := dal.MenuDAO.Query(f)
	if err != nil {
		return nil, err
	}

	return (*dtoex.MenuQueryRes)(res).Extend(), nil
}

func (srv *menuSrv) Get(id int) (*models.TMenu, error) {
	return dal.MenuDAO.Get(id)
}

func (srv *menuSrv) GetMulti(idList []int) (map[int]models.TMenu, error) {
	return dal.MenuDAO.GetMulti(idList)
}

func (srv *menuSrv) GetMenuTree(form *dto.MenuQueryForm) (*treetool.MenuTreeNode, error) {
	list, err := dal.MenuDAO.GetList(form)
	if err != nil {
		return nil, err
	}
	exList := dtoex.ExtendMenuList(list)

	tree := treetool.NewMenuTree(exList, 0)
	return tree.GetTree(), nil
}

func (srv *menuSrv) GetMenuSelectOptions(systemId int, treeRootId int) ([]dtoex.TMenuEx, error) {
	form := &dto.MenuQueryForm{
		TMenu: models.TMenu{
			SystemId: systemId,
		},
		PageForm: dto.PageForm{
			Page:  1,
			Psize: 10000,
		},
	}
	list, err := dal.MenuDAO.GetList(form)
	if err != nil {
		return nil, err
	}
	exList := dtoex.ExtendMenuList(list)

	tree := treetool.NewMenuTree(exList, treeRootId)
	return tree.GetSelectOptions(), nil
}

func (srv *menuSrv) Insert(m *models.TMenu) error {
	return dal.MenuDAO.Insert(m)
}

func (srv *menuSrv) Update(m *models.TMenu) error {
	return dal.MenuDAO.Update(m)
}

func (srv *menuSrv) Delete(id int) error {
	return dal.MenuDAO.Delete(id)
}

func (srv *menuSrv) RealDelete(id int) error {
	return dal.MenuDAO.RealDelete(id)
}
