package rbac

import (
	"go-admin-auth/app/dal"
	"go-admin-auth/app/dml"
	"go-admin-auth/app/models"
	"go-admin-auth/gin_helper/define"
	"go-admin-auth/httpapi/dto"
)

type rbacMenuSrv struct{}

var (
	RBACMenuSrv = &rbacMenuSrv{}
)

func (r *rbacMenuSrv) GetMenuIdListByUserId(userId int) ([]int, error) {
	user, _ := dml.UserDML.Get(userId)
	if user.IsRoot == define.RootUserTag {
		return nil, nil
	}

	roleIdList, err := RBACRoleSrv.GetRoleIdListByUserId(userId)
	if err != nil {
		return nil, err
	}

	roleMenuList, err := r.GetMenuListByRoleIdList(roleIdList)
	if err != nil {
		return nil, err
	}

	menuIdList := make([]int, 0)
	for _, v := range roleMenuList {
		menuIdList = append(menuIdList, v.MenuId)
	}

	return menuIdList, nil
}

func (r *rbacMenuSrv) GetMenuListByRoleIdList(roleIdList []int) ([]*models.TRoleMenu, error) {
	form := new(dto.RoleMenuQueryForm)
	form.Page = 1
	form.Psize = 10000
	form.RoleIdList = roleIdList

	return dal.RoleMenuDAO.GetList(form)
}
