package rbac

import (
	"go-admin-auth/app/dal"
	"go-admin-auth/app/models"
	"go-admin-auth/httpapi/dto"
)

type rbacRoleSrv struct{}

var (
	RBACRoleSrv = &rbacRoleSrv{}
)

func (r *rbacRoleSrv) GetRoleIdListByUserId(userId int) ([]int, error) {
	roleList, err := r.GetUserRoleListByUserId(userId)
	if err != nil {
		return nil, err
	}

	roleIdList := make([]int, 0)
	for _, v := range roleList {
		roleIdList = append(roleIdList, v.RoleId)
	}

	return roleIdList, nil
}

func (r *rbacRoleSrv) GetUserRoleListByUserId(userId int) ([]models.TUserRole, error) {
	form := new(dto.UserRoleQueryForm)
	form.Page = 1
	form.Psize = 10000
	form.UserId = userId

	return dal.UserRoleDAO.GetList(form)
}
