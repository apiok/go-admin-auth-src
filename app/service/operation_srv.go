package service

import (
	"go-admin-auth/app/dal"
	"go-admin-auth/app/models"
	"go-admin-auth/httpapi/dto"
	"go-admin-auth/httpapi/dtoex"
)

type operationSrv struct{}

var (
	OperationSrv = &operationSrv{}
)

func (srv *operationSrv) Query(f *dto.OperationQueryForm) (*dtoex.OperationQueryResEx, error) {
	res, err := dal.OperationDAO.Query(f)
	if err != nil {
		return nil, err
	}
	return (*dtoex.OperationQueryRes)(res).Extend(), nil
}

func (srv *operationSrv) Get(id int) (*models.TOperation, error) {
	return dal.OperationDAO.Get(id)
}

func (srv *operationSrv) GetListBySystemId(systemId int) ([]*models.TOperation, error) {
	form := new(dto.OperationQueryForm)
	form.PageForm = dto.PageForm{
		Page: 1,
		Psize: 10000,
	}
	form.TOperation = models.TOperation{
		SystemId: systemId,
	}


	return dal.OperationDAO.GetList(form)
}

func (srv *operationSrv) GetListByMenuId(menuId int) ([]*models.TOperation, error) {
	form := new(dto.OperationQueryForm)
	form.PageForm = dto.PageForm{
		Page: 1,
		Psize: 10000,
	}
	form.TOperation = models.TOperation{
		MenuId: menuId,
	}


	return dal.OperationDAO.GetList(form)
}

func (srv *operationSrv) GetMulti(idList []int) (map[int]models.TOperation, error) {
	return dal.OperationDAO.GetMulti(idList)
}

func (srv *operationSrv) Insert(m *models.TOperation) error {
	menu, _ := dal.MenuDAO.Get(m.MenuId)
	m.ParentId = menu.ParentId
	return dal.OperationDAO.Insert(m)
}

func (srv *operationSrv) Update(m *models.TOperation) error {
	menu, _ := dal.MenuDAO.Get(m.MenuId)
	m.ParentId = menu.ParentId
	return dal.OperationDAO.Update(m)
}

func (srv *operationSrv) Delete(id int) error {
	return dal.OperationDAO.Delete(id)
}

func (srv *operationSrv) RealDelete(id int) error {
	return dal.OperationDAO.RealDelete(id)
}
