package service

import (
	"go-admin-auth/app/dal"
	"go-admin-auth/app/models"
	"go-admin-auth/httpapi/dto"
	"go-admin-auth/httpapi/dtoex"
)

type roleMenuSrv struct{}

var (
	RoleMenuSrv = &roleMenuSrv{}
)

func (srv *roleMenuSrv) Query(f *dto.RoleMenuQueryForm) (*dtoex.RoleMenuQueryResEx, error) {
	res, err := dal.RoleMenuDAO.Query(f)
	if err != nil {
		return nil, err
	}

	return (*dtoex.RoleMenuQueryRes)(res).Extend(), nil
}

func (srv *roleMenuSrv) Get(id int) (*models.TRoleMenu, error) {
	return dal.RoleMenuDAO.Get(id)
}

func (srv *roleMenuSrv) GetMulti(idList []int) (map[int]models.TRoleMenu, error)  {
	return dal.RoleMenuDAO.GetMulti(idList)
}

func (srv *roleMenuSrv) Insert(m *models.TRoleMenu) error {
	return dal.RoleMenuDAO.Insert(m)
}

func (srv *roleMenuSrv) Update(m *models.TRoleMenu) error {
	return dal.RoleMenuDAO.Update(m)
}

func (srv *roleMenuSrv) Delete(id int) error {
	return dal.RoleMenuDAO.Delete(id)
}

func (srv *roleMenuSrv) RealDelete(id int) error {
	return dal.RoleMenuDAO.RealDelete(id)
}