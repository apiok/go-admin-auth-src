package service

import (
	"go-admin-auth/app/dal"
	"go-admin-auth/app/models"
	"go-admin-auth/httpapi/dto"
	"go-admin-auth/httpapi/dtoex"
)

type roleSrv struct{}

var (
	RoleSrv = &roleSrv{}
)

func (srv *roleSrv) Query(f *dto.RoleQueryForm) (*dtoex.RoleQueryResEx, error) {
	res, err := dal.RoleDAO.Query(f)
	if err != nil {
		return nil, err
	}

	return (*dtoex.RoleQueryRes)(res).Extend(), nil
}

func (srv *roleSrv) Get(id int) (*models.TRole, error) {
	return dal.RoleDAO.Get(id)
}

func (srv *roleSrv) GetMulti(idList []int) (map[int]models.TRole, error)  {
	return dal.RoleDAO.GetMulti(idList)
}

func (srv *roleSrv) Insert(m *models.TRole) error {
	return dal.RoleDAO.Insert(m)
}

func (srv *roleSrv) Update(m *models.TRole) error {
	return dal.RoleDAO.Update(m)
}

func (srv *roleSrv) Delete(id int) error {
	return dal.RoleDAO.Delete(id)
}

func (srv *roleSrv) RealDelete(id int) error {
	return dal.RoleDAO.RealDelete(id)
}