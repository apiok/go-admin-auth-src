package service

import (
	"go-admin-auth/app/dal"
	"go-admin-auth/app/models"
	"go-admin-auth/httpapi/dto"
	"go-admin-auth/httpapi/dtoex"
)

type subsystemSrv struct{}

var (
	SubsystemSrv = &subsystemSrv{}
)

func (srv *subsystemSrv) Query(f *dto.SubsystemQueryForm) (*dtoex.SubsystemQueryResEx, error) {
	res, err := dal.SubsystemDAO.Query(f)
	if err != nil {
		return nil, err
	}

	return (*dtoex.SubsystemQueryRes)(res).Extend(), nil
}

func (srv *subsystemSrv) Get(id int) (*models.TSubsystem, error) {
	return dal.SubsystemDAO.Get(id)
}

func (srv *subsystemSrv) GetMulti(idList []int) (map[int]models.TSubsystem, error)  {
	return dal.SubsystemDAO.GetMulti(idList)
}

func (srv *subsystemSrv) Insert(m *models.TSubsystem) error {
	return dal.SubsystemDAO.Insert(m)
}

func (srv *subsystemSrv) Update(m *models.TSubsystem) error {
	return dal.SubsystemDAO.Update(m)
}

func (srv *subsystemSrv) Delete(id int) error {
	return dal.SubsystemDAO.Delete(id)
}

func (srv *subsystemSrv) RealDelete(id int) error {
	return dal.SubsystemDAO.RealDelete(id)
}