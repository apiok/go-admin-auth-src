package org

import (
	"fmt"
	"go-admin-auth/app/models"
	"go-admin-auth/app/service"
)

type orgCtl struct {
}

var OrgCtl = new(orgCtl)

func (o orgCtl) GetSubDepartIdListByOrgId(orgId int) ([]int, error) {
	orgInfo, err := service.OrganizationSrv.Get(orgId)
	if err != nil {
		return nil, err
	}

	orgList, err := service.OrganizationSrv.GetSubList(fmt.Sprintf("%v%v/", orgInfo.Path, orgInfo.Id))
	if err != nil {
		return nil, nil
	}

	orgIdList := make([]int, 0, len(orgList))
	for _, v := range orgList {
		orgIdList = append(orgIdList, v.Id)
	}
	return orgIdList, nil
}

func (o orgCtl) GetSubUserListByOrgId(orgId int) ([]models.TUser, error) {
	orgIdList, err := o.GetSubDepartIdListByOrgId(orgId)
	if err != nil {
		return nil, nil
	}

	return service.UserSrv.GetListByOrgIdList(orgIdList)
}

func (o orgCtl) GetSubUserListByUserId(userId int) ([]models.TUser, error) {
	userInfo, err := service.UserSrv.Get(userId)
	if err != nil {
		return nil, nil
	}

	return o.GetSubUserListByOrgId(userInfo.OrgId)
}

func (o orgCtl) GetSubUserIdListByUserId(userId int) ([]int, error) {
	userInfo, err := service.UserSrv.Get(userId)
	if err != nil {
		return nil, nil
	}

	userList, err := o.GetSubUserListByOrgId(userInfo.OrgId)
	if err != nil {
		return nil, nil
	}

	userIdList := make([]int, len(userList), len(userList))
	for i, v := range userList {
		userIdList[i] = v.Id
	}
	return userIdList, nil
}
