package service

import (
	"go-admin-auth/httpapi/dto"
	"go-admin-auth/httpapi/enums"
)

type enumSrv struct{}

var (
	EnumSrv = &enumSrv{}
)

func (srv *enumSrv) GetCommonStatusOptions() []dto.EnumIntOption {
	optionList := make([]dto.EnumIntOption, 0)
	for i, v := range enums.StatusMap {
		option := dto.EnumIntOption{
			Value: i,
			Name:  v,
		}
		optionList = append(optionList, option)
	}

	return optionList
}

func (srv *enumSrv) GetCommonYesOrNoOptions() []dto.EnumIntOption {
	optionList := make([]dto.EnumIntOption, 0)
	for i, v := range enums.YesOrNoMap {
		option := dto.EnumIntOption{
			Value: i,
			Name:  v,
		}
		optionList = append(optionList, option)
	}

	return optionList
}

func (srv *enumSrv) GetOrgStatusOptions() []dto.EnumIntOption {
	optionList := make([]dto.EnumIntOption, 0)
	for i, v := range enums.StatusMap {
		option := dto.EnumIntOption{
			Value: i,
			Name:  v,
		}
		optionList = append(optionList, option)
	}

	return optionList
}

func (srv *enumSrv) GetUserIsRootOptions() []dto.EnumIntOption {
	optionList := make([]dto.EnumIntOption, 0)
	for i, v := range enums.YesOrNoMap {
		option := dto.EnumIntOption{
			Value: i,
			Name:  v,
		}
		optionList = append(optionList, option)
	}

	return optionList
}

func (srv *enumSrv) GetUserIsStuffOptions() []dto.EnumIntOption {
	optionList := make([]dto.EnumIntOption, 0)
	for i, v := range enums.YesOrNoMap {
		option := dto.EnumIntOption{
			Value: i,
			Name:  v,
		}
		optionList = append(optionList, option)
	}

	return optionList
}
