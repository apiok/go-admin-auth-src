package service

import (
	"go-admin-auth/app/dal"
	"go-admin-auth/app/lib/treetool"
	"go-admin-auth/app/models"
	"go-admin-auth/httpapi/dto"
)

type menuOperaPermSrv struct{}

var (
	MenuOperaPermSrv = &menuOperaPermSrv{}
)

func (srv *menuOperaPermSrv) Get(systemId int) (*treetool.MenuOperaTreeNode, error) {
	form := new(dto.MenuQueryForm)
	form.PageForm = dto.PageForm{
		Page:  1,
		Psize: 10000,
	}
	form.TMenu = models.TMenu{
		SystemId: systemId,
	}

	res := make([]*dto.MenuOperaPermDTO, 0)

	menuList, err := dal.MenuDAO.GetList(form)
	if err != nil {
		return nil, err
	}

	for _, menu := range menuList {
		operaList, _ := OperationSrv.GetListByMenuId(menu.Id)

		item := new(dto.MenuOperaPermDTO)
		item.TMenu = menu
		item.OperationList = operaList
		res = append(res, item)
	}


	tree := treetool.NewMenuOperaTree(res, 0)

	return tree.GetTree(), nil
}
