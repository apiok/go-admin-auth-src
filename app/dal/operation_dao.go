package dal

import (
	"go-admin-auth/app/models"
	"go-admin-auth/conn/mysql_conn"
	"go-admin-auth/httpapi/dto"
	"gorm.io/gorm"
	"time"
)

type operationDAO struct{}

var (
	OperationDAO = &operationDAO{}
)

func (d *operationDAO) Query(f *dto.OperationQueryForm) (*dto.OperationQueryRes, error) {
	session := d.newSession()

	var total int64
	var list []models.TOperation

	if len(f.OrderBy) > 0 {
		for _, v := range f.OrderBy {
			session.Order(v)
		}
	}

	if f.SystemId != 0 {
		session.Where("system_id = ?", f.SystemId)
	}

	if f.ParentId != 0 {
		session.Where("parent_id = ?", f.ParentId)
	}

	if f.MenuId != 0 {
		session.Where("menu_id = ?", f.MenuId)
	}

	if f.Name != "" {
		session.Where("name like ?", f.Name)
	}

	if f.SearchMenuId != 0 {
		session.Where("parent_id = ? or menu_id= ?", f.SearchMenuId, f.SearchMenuId)
	}

	if err := session.Count(&total).Error; err != nil {
		return nil, err
	}
	if err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&list).Error; err != nil {
		return nil, err
	}

	return &dto.OperationQueryRes{Total: total, List: list}, nil
}

func (d *operationDAO) GetList(f *dto.OperationQueryForm) ([]*models.TOperation, error) {
	var results []*models.TOperation

	session := d.newSession()

	if f.SystemId != 0 {
		session.Where("system_id = ?", f.SystemId)
	}

	if f.ParentId != 0 {
		session.Where("parent_id = ?", f.ParentId)
	}

	if f.MenuId != 0 {
		session.Where("menu_id = ?", f.MenuId)
	}


	err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&results).Error
	if err != nil {
		return nil, err
	}
	return results, nil
}

func (d *operationDAO) Get(id int) (*models.TOperation, error) {
	info := &models.TOperation{}
	session := d.newSession()
	if err := session.Where("id= ?", id).First(info).Error; err != nil {
		return nil, err
	}

	return info, nil
}

func (d *operationDAO) GetMulti(idList []int) (map[int]models.TOperation, error) {
	var mMap = make(map[int]models.TOperation, 0)
	var results []models.TOperation

	session := d.newSession()
	query := session.Where("id IN ?", idList)
	err := query.Find(&results).Error
	if err != nil {
		return nil, err
	}
	for _, v := range results {
		mMap[v.Id] = v
	}
	return mMap, nil
}

func (d *operationDAO) Insert(m *models.TOperation) error {
	session := d.newSession()
	err := session.Create(m).Error
	return err
}

func (d *operationDAO) Update(m *models.TOperation) error {
	session := d.newSession()
	err := session.Where("id = ?", m.Id).UpdateColumns(m).Error
	return err
}

func (d *operationDAO) Delete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Updates(map[string]interface{}{"is_deleted": 1, "deleted_at": time.Now()}).Error
	return err
}

func (d *operationDAO) RealDelete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Delete(models.TOperation{}).Error
	return err
}

func (d *operationDAO) newEngine() *gorm.DB {
	return mysql_conn.GetDb()
}

func (d *operationDAO) newSession() *gorm.DB {
	return mysql_conn.GetSession().Table("t_operation")
}
