package dal

import (
	"go-admin-auth/app/models"
	"go-admin-auth/conn/mysql_conn"
	"go-admin-auth/httpapi/dto"
	"gorm.io/gorm"
	"time"
)

type organizationDAO struct{}

var (
	OrganizationDAO = &organizationDAO{}
)

func (d *organizationDAO) Query(f *dto.OrganizationQueryForm) (*dto.OrganizationQueryRes, error) {
	session := d.newSession()

	var total int64
	var list []models.TOrganization

	if len(f.OrderBy) > 0 {
		for _, v := range f.OrderBy {
			session.Order(v)
		}
	}

	if !(f.ParentPath == "/" || f.ParentPath == "") {
		session.Where("path like ?", f.ParentPath+"%")
		//session.Where("instr(`path`, ?)", fmt.Sprintf("/%v/", f.ParentPath))
	}

	if f.Name != "" {
		session.Where("name like ?", f.Name)
	}

	if err := session.Count(&total).Error; err != nil {
		return nil, err
	}
	if err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&list).Error; err != nil {
		return nil, err
	}

	return &dto.OrganizationQueryRes{Total: total, List: list}, nil
}

func (d *organizationDAO) GetList(f *dto.OrganizationQueryForm) ([]models.TOrganization, error) {
	var results []models.TOrganization

	session := d.newSession()

	if f.ParentId != 0 {
		session.Where("parent_id = ?", f.ParentId)
	}

	err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&results).Error
	if err != nil {
		return nil, err
	}
	return results, nil
}

func (d *organizationDAO) Get(id int) (*models.TOrganization, error) {
	info := &models.TOrganization{}
	session := d.newSession()
	if err := session.Where("id= ?", id).First(info).Error; err != nil {
		return nil, err
	}

	return info, nil
}

func (d *organizationDAO) GetMulti(idList []int) (map[int]models.TOrganization, error) {
	var mMap = make(map[int]models.TOrganization, 0)
	var results []models.TOrganization

	session := d.newSession()
	query := session.Where("id IN ?", idList)
	err := query.Find(&results).Error
	if err != nil {
		return nil, err
	}
	for _, v := range results {
		mMap[v.Id] = v
	}
	return mMap, nil
}

func (d *organizationDAO) Insert(m *models.TOrganization) error {
	session := d.newSession()
	err := session.Create(m).Error
	return err
}

func (d *organizationDAO) Update(m *models.TOrganization) error {
	session := d.newSession()
	err := session.Where("id = ?", m.Id).UpdateColumns(m).Error
	return err
}

func (d *organizationDAO) Delete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Updates(map[string]interface{}{"is_deleted": 1, "deleted_at": time.Now()}).Error
	return err
}

func (d *organizationDAO) RealDelete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Delete(models.TOrganization{}).Error
	return err
}

func (d *organizationDAO) newEngine() *gorm.DB {
	return mysql_conn.GetDb()
}

func (d *organizationDAO) newSession() *gorm.DB {
	return mysql_conn.GetSession().Table("t_organization")
}
