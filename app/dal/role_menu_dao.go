package dal

import (
	"go-admin-auth/app/models"
	"go-admin-auth/conn/mysql_conn"
	"go-admin-auth/httpapi/dto"
	"gorm.io/gorm"
	"time"
)

type roleMenuDAO struct{}

var (
	RoleMenuDAO = &roleMenuDAO{}
)

func (d *roleMenuDAO) Query(f *dto.RoleMenuQueryForm) (*dto.RoleMenuQueryRes, error) {
	session := d.newSession()

	var total int64
	var list []models.TRoleMenu

	if len(f.RoleIdList) > 0 {
		session.Where("role_id in (?)", f.RoleIdList)
	}

	if len(f.OrderBy) > 0 {
		for _, v := range f.OrderBy {
			session.Order(v)
		}
	}

	if err := session.Count(&total).Error; err != nil {
		return nil, err
	}
	if err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&list).Error; err != nil {
		return nil, err
	}

	return &dto.RoleMenuQueryRes{Total: total, List: list}, nil
}

func (d *roleMenuDAO) GetList(f *dto.RoleMenuQueryForm) ([]*models.TRoleMenu, error) {
	var results []*models.TRoleMenu

	session := d.newSession()

	if f.RoleId != 0 {
		session.Where("role_id = ?", f.RoleId)
	}

	if f.SystemId != 0 {
		session.Where("system_id = ?", f.SystemId)
	}

	if f.MenuId != 0 {
		session.Where("menu_id = ?", f.MenuId)
	}

	err := session.Find(&results).Error
	if err != nil {
		return nil, err
	}
	return results, nil
}

func (d *roleMenuDAO) Get(id int) (*models.TRoleMenu, error) {
	info := &models.TRoleMenu{}
	session := d.newSession()
	if err := session.Where("id= ?", id).First(info).Error; err != nil {
		return nil, err
	}

	return info, nil
}

func (d *roleMenuDAO) GetMulti(idList []int) (map[int]models.TRoleMenu, error) {
	var mMap = make(map[int]models.TRoleMenu, 0)
	var results []models.TRoleMenu

	session := d.newSession()
	query := session.Where("id IN ?", idList)
	err := query.Find(&results).Error
	if err != nil {
		return nil, err
	}
	for _, v := range results {
		mMap[v.Id] = v
	}
	return mMap, nil
}

func (d *roleMenuDAO) BatchInsert(m []*models.TRoleMenu) error {
	session := d.newSession()
	err := session.Create(m).Error
	return err
}

func (d *roleMenuDAO) Insert(m *models.TRoleMenu) error {
	session := d.newSession()
	err := session.Create(m).Error
	return err
}

func (d *roleMenuDAO) Update(m *models.TRoleMenu) error {
	session := d.newSession()
	err := session.Where("id = ?", m.Id).UpdateColumns(m).Error
	return err
}

func (d *roleMenuDAO) Delete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Updates(map[string]interface{}{"is_deleted": 1, "deleted_at": time.Now()}).Error
	return err
}

func (d *roleMenuDAO) RealDeleteByRoleId(roleId int) error {
	session := d.newSession()
	err := session.Where("role_id = ?", roleId).Delete(models.TRoleMenu{}).Error
	return err
}

func (d *roleMenuDAO) RealDelete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Delete(models.TRoleMenu{}).Error
	return err
}

func (d *roleMenuDAO) newEngine() *gorm.DB {
	return mysql_conn.GetDb()
}

func (d *roleMenuDAO) newSession() *gorm.DB {
	return mysql_conn.GetSession().Table("t_role_menu")
}
