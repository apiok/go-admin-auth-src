package dal

import (
	"go-admin-auth/app/models"
	"go-admin-auth/conn/mysql_conn"
	"go-admin-auth/httpapi/dto"
	"gorm.io/gorm"
	"time"
)

type subsystemDAO struct{}

var (
	SubsystemDAO = &subsystemDAO{}
)

func (d *subsystemDAO) Query(f *dto.SubsystemQueryForm) (*dto.SubsystemQueryRes, error) {
	session := d.newSession()

	var total int64
	var list []models.TSubsystem

	if f.Name != "" {
		session.Where("name like ?", f.Name)
	}

	if f.Domain != "" {
		session.Where("domain like ?", f.Domain)
	}

	if len(f.OrderBy) > 0 {
		for _, v := range f.OrderBy {
			session.Order(v)
		}
	}

	if err := session.Count(&total).Error; err != nil {
		return nil, err
	}
	if err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&list).Error; err != nil {
		return nil, err
	}

	return &dto.SubsystemQueryRes{Total: total, List: list}, nil
}

func (d *subsystemDAO) GetList(f *dto.SubsystemQueryForm) ([]*models.TSubsystem, error) {
	var results []*models.TSubsystem

	session := d.newSession()

	err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&results).Error
	if err != nil {
		return nil, err
	}
	return results, nil
}

func (d *subsystemDAO) Get(id int) (*models.TSubsystem, error) {
	info := &models.TSubsystem{}
	session := d.newSession()
	if err := session.Where("id= ?", id).First(info).Error; err != nil {
		return nil, err
	}

	return info, nil
}

func (d *subsystemDAO) GetMulti(idList []int) (map[int]models.TSubsystem, error) {
	var mMap = make(map[int]models.TSubsystem, 0)
	var results []models.TSubsystem

	session := d.newSession()
	query := session.Where("id IN ?", idList)
	err := query.Find(&results).Error
	if err != nil {
		return nil, err
	}
	for _, v := range results {
		mMap[v.Id] = v
	}
	return mMap, nil
}

func (d *subsystemDAO) Insert(m *models.TSubsystem) error {
	session := d.newSession()
	err := session.Create(m).Error
	return err
}

func (d *subsystemDAO) Update(m *models.TSubsystem) error {
	session := d.newSession()
	err := session.Where("id = ?", m.Id).UpdateColumns(m).Error
	return err
}

func (d *subsystemDAO) Delete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Updates(map[string]interface{}{"is_deleted": 1, "deleted_at": time.Now()}).Error
	return err
}

func (d *subsystemDAO) RealDelete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Delete(models.TSubsystem{}).Error
	return err
}

func (d *subsystemDAO) newEngine() *gorm.DB {
	return mysql_conn.GetDb()
}

func (d *subsystemDAO) newSession() *gorm.DB {
	return mysql_conn.GetSession().Table("t_subsystem")
}
