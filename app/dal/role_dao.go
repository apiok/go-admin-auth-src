package dal

import (
	"go-admin-auth/app/models"
	"go-admin-auth/conn/mysql_conn"
	"go-admin-auth/httpapi/dto"
	"gorm.io/gorm"
	"time"
)

type roleDAO struct{}

var (
	RoleDAO = &roleDAO{}
)

func (d *roleDAO) Query(f *dto.RoleQueryForm) (*dto.RoleQueryRes, error) {
	session := d.newSession()

	var total int64
	var list []models.TRole

	if f.Name != "" {
		session.Where("name like ?", f.Name)
	}

	if len(f.OrderBy) > 0 {
		for _, v := range f.OrderBy {
			session.Order(v)
		}
	}

	if err := session.Count(&total).Error; err != nil {
		return nil, err
	}
	if err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&list).Error; err != nil {
		return nil, err
	}

	return &dto.RoleQueryRes{Total: total, List: list}, nil
}

func (d *roleDAO) GetList(f *dto.RoleQueryForm) ([]*models.TRole, error) {
	var results []*models.TRole

	session := d.newSession()

	err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&results).Error
	if err != nil {
		return nil, err
	}
	return results, nil
}

func (d *roleDAO) Get(id int) (*models.TRole, error) {
	info := &models.TRole{}
	session := d.newSession()
	if err := session.Where("id= ?", id).First(info).Error; err != nil {
		return nil, err
	}

	return info, nil
}

func (d *roleDAO) GetMulti(idList []int) (map[int]models.TRole, error) {
	var mMap = make(map[int]models.TRole, 0)
	var results []models.TRole

	session := d.newSession()
	query := session.Where("id IN ?", idList)
	err := query.Find(&results).Error
	if err != nil {
		return nil, err
	}
	for _, v := range results {
		mMap[v.Id] = v
	}
	return mMap, nil
}

func (d *roleDAO) Insert(m *models.TRole) error {
	session := d.newSession()
	err := session.Create(m).Error
	return err
}

func (d *roleDAO) Update(m *models.TRole) error {
	session := d.newSession()
	err := session.Where("id = ?", m.Id).UpdateColumns(m).Error
	return err
}

func (d *roleDAO) Delete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Updates(map[string]interface{}{"is_deleted": 1, "deleted_at": time.Now()}).Error
	return err
}

func (d *roleDAO) RealDelete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Delete(models.TRole{}).Error
	return err
}

func (d *roleDAO) newEngine() *gorm.DB {
	return mysql_conn.GetDb()
}

func (d *roleDAO) newSession() *gorm.DB {
	return mysql_conn.GetSession().Table("t_role")
}
