package dal

import (
	"go-admin-auth/app/models"
	"go-admin-auth/conn/mysql_conn"
	"go-admin-auth/httpapi/dto"
	"gorm.io/gorm"
	"time"
)

type menuDAO struct{}

var (
	MenuDAO = &menuDAO{}
)

func (d *menuDAO) Query(f *dto.MenuQueryForm) (*dto.MenuQueryRes, error) {
	session := d.newSession()

	var total int64
	var list []models.TMenu

	if f.SystemId != 0 {
		session.Where("system_id = ?", f.SystemId)
	}

	if f.ParentId != 0 {
		session.Where("parent_id = ?", f.ParentId)
	}

	if f.Name != "" {
		session.Where("name like ?", f.Name)
	}

	if len(f.OrderBy) > 0 {
		for _, v := range f.OrderBy {
			session.Order(v)
		}
	}

	if err := session.Count(&total).Error; err != nil {
		return nil, err
	}
	if err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&list).Error; err != nil {
		return nil, err
	}

	return &dto.MenuQueryRes{Total: total, List: list}, nil
}

func (d *menuDAO) GetList(f *dto.MenuQueryForm) ([]models.TMenu, error) {
	var results []models.TMenu

	session := d.newSession()

	if f.SystemId != 0 {
		session.Where("system_id = ?", f.SystemId)
	}

	if len(f.IdList) > 0 {
		session.Where("id in (?)", f.IdList)
	}

	if f.Ids != "" {
		session.Where("id in (?)", f.Ids)
	}

	err := session.Limit(f.Limit()).Offset(f.Offset()).Find(&results).Error
	if err != nil {
		return nil, err
	}
	return results, nil
}

func (d *menuDAO) Get(id int) (*models.TMenu, error) {
	info := &models.TMenu{}
	session := d.newSession()
	if err := session.Where("id= ?", id).First(info).Error; err != nil {
		return nil, err
	}

	return info, nil
}

func (d *menuDAO) GetMulti(idList []int) (map[int]models.TMenu, error) {
	var mMap = make(map[int]models.TMenu, 0)
	var results []models.TMenu

	session := d.newSession()
	query := session.Where("id IN ?", idList)
	err := query.Find(&results).Error
	if err != nil {
		return nil, err
	}
	for _, v := range results {
		mMap[v.Id] = v
	}
	return mMap, nil
}

func (d *menuDAO) Insert(m *models.TMenu) error {
	session := d.newSession()
	err := session.Create(m).Error
	return err
}

func (d *menuDAO) Update(m *models.TMenu) error {
	session := d.newSession()
	err := session.Where("id = ?", m.Id).UpdateColumns(m).Error
	return err
}

func (d *menuDAO) Delete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Updates(map[string]interface{}{"is_deleted": 1, "deleted_at": time.Now()}).Error
	return err
}

func (d *menuDAO) RealDelete(id int) error {
	session := d.newSession()
	err := session.Where("id = ?", id).Delete(models.TMenu{}).Error
	return err
}

func (d *menuDAO) newEngine() *gorm.DB {
	return mysql_conn.GetDb()
}

func (d *menuDAO) newSession() *gorm.DB {
	return mysql_conn.GetSession().Table("t_menu")
}
