package conf

import (
	"strconv"
	"syscall"
)

func GetEnvDft(key string, dft string) string {
	v, ok := syscall.Getenv(key)
	if !ok {
		return dft
	}

	return v
}

func GetEnvIntDft(key string, dft int) int {
	v, ok := syscall.Getenv(key)
	if !ok {
		return dft
	}

	vInt, err := strconv.Atoi(v)
	if err != nil {
		return dft
	}
	return vInt
}

func GetEnvInt(key string) int {
	v, ok := syscall.Getenv(key)
	if !ok {
		return 0
	}

	vInt, err := strconv.Atoi(v)
	if err != nil {
		return 0
	}
	return vInt
}
