package conf

import (
	"github.com/joho/godotenv"
	"log"
	"syscall"
)

var (
	Theme = "default"
)

func init() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Print("Error loading .env file")
	}

	if v, ok := syscall.Getenv("APP_THEME"); ok {
		Theme = v
	}

}
