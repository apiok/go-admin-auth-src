package gin_helper

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/gin_helper/define"
	"net/http"
)

func JsonOk(c *gin.Context, data interface{}) {
	JsonRaw(c, define.SUCCESS, define.StatusText(define.SUCCESS), data)
}

func JsonErr(c *gin.Context, e error) {
	JsonRaw(c, define.ERROR, e.Error(), nil)
}

func JsonFail(c *gin.Context, code int, msg string) {
	JsonRaw(c, code, msg, nil)
}

func JsonRaw(c *gin.Context, code int, msg string, data interface{}) {
	c.JSON(http.StatusOK, gin.H{"code": code, "msg": msg, "data": data})
}
