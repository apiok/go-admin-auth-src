package define

const (
	RootUserTag = 1

	// admin token 相关

	AdminUserKey      = "admin_id"
	AdminTokenKey     = "AdminAuthorization"
	AdminJwtSecretKey = "ADMIN_JWT_SECRET"

	// inner token 相关
	InnerSystemId     = "system_id"
	InnerTokenKey     = "Inner-Authorization"
	InnerJwtSecretKey = "INNER_JWT_SECRET"
)
