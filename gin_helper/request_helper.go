package gin_helper

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"go-admin-auth/app/lib"
	"go-admin-auth/gin_helper/define"
	"strconv"
	"strings"
)

func ParamInt(c *gin.Context, key string) (int, error) {
	v := c.Param(key)
	return strconv.Atoi(v)
}

func ParamStrList(c *gin.Context, key string) ([]string, error) {
	v := c.Param(key)

	return strings.Split(v, ","), nil
}

func ParamIntList(c *gin.Context, key string) ([]int, error) {
	v := c.Param(key)

	strS := strings.Split(v, ",")
	return lib.StringSliceToIntSlice(strS)
}

func RestParams(c *gin.Context) (map[string]interface{}, error) {
	if len(c.Params) == 0 {
		return nil, errors.New("no params")
	}

	var params = make(map[string]interface{})
	for key, values := range c.Request.Form {
		if len(values) > 1 {
			return nil, fmt.Errorf("key value lg 1, key:%v", key)
		}
		params[key] = values[0]
	}

	return params, nil
}

func PureRestParams(c *gin.Context) (map[string]interface{}, error) {
	params, err := RestParams(c)
	if err != nil {
		return params, err
	}

	for k, _ := range params {
		switch k {
		case "page", "psize", "orderBy", "orderDirect", "rest_method_xyz":
			delete(params, k)
		}
	}

	return params, nil
}

func GetCurrentUserId(c *gin.Context) int {
	userId, ok := c.Get(define.AdminUserKey)
	if !ok {
		return 0
	}
	return cast.ToInt(userId)
}
