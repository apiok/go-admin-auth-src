package router

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/http/handler/admin"
	"go-admin-auth/app/http/middleware"
)

func innerRoute(engine *gin.Engine) {
	adminOuterAPI := engine.Group("/outer-api/v1")
	{
		adminOuterAPI.POST("/checkInnerToken", admin.TokenHandler.CheckInnerToken)
		adminOuterAPI.POST("/checkAdminToken", admin.TokenHandler.CheckAdminToken)
	}

	adminInnerAPI := engine.Group("/inner-api/v1").Use(middleware.InnerToken)
	{
		//adminInnerAPI.POST("/token", admin.AuthHandler.Login)
		//adminInnerAPI.DELETE("/token", admin.AuthHandler.Logout)

		adminInnerAPI.GET("/sidebar", admin.MenuToolHandler.GetSideBarTree)
		adminInnerAPI.GET("/enum_options", admin.EnumHandler.GetOptions)

		adminInnerAPI.GET("/user", admin.UserHandler.Query)
		adminInnerAPI.GET("/user/:id", admin.UserHandler.Get)
		adminInnerAPI.GET("/userGetMulti", admin.UserHandler.GetMulti)

		adminInnerAPI.GET("/get_sub_user_id_list", admin.OrgPermHandler.GetSubUserIdList) // 获取 下级用户Id列表
		adminInnerAPI.GET("/get_sub_user_list", admin.OrgPermHandler.GetSubUserList)      // 获取 下级用户Id列表
	}
}
