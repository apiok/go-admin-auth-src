package router

import (
	"github.com/gin-gonic/gin"
	"go-admin-auth/app/http/handler/admin"
	"go-admin-auth/app/http/middleware"
)

func adminRoute(engine *gin.Engine) {
	adminOpenAPI := engine.Group("/admin-api/v1")
	{
		adminOpenAPI.POST("/token", admin.AuthHandler.Login)
		adminOpenAPI.DELETE("/token", admin.AuthHandler.Logout)
	}

	cacheAPI := engine.Group("/admin-api/v1")
	cacheAPI.Use(middleware.AdminToken)
	cacheAPI.Use(middleware.BrowserCacheMiddleware)
	{
		// 菜单树
		cacheAPI.GET("/sidebar", admin.MenuToolHandler.GetSideBarTree)
	}

	adminAPI := engine.Group("/admin-api/v1")
	adminAPI.Use(middleware.AdminToken)
	// 框架菜单相关
	{
		// 菜单列表
		adminAPI.GET("/menu", admin.MenuHandler.Query)
		adminAPI.GET("/menu/:id", admin.MenuHandler.Get)
		adminAPI.POST("/menu", admin.MenuHandler.Add)
		adminAPI.PUT("/menu/:id", admin.MenuHandler.Edit)
		adminAPI.DELETE("/menu/:id", admin.MenuHandler.Delete)

		adminAPI.GET("/menu_options", admin.MenuToolHandler.GetSelectOptions)

		adminAPI.GET("/operation", admin.OperationHandler.Query)
		adminAPI.GET("/operation/:id", admin.OperationHandler.Get)
		adminAPI.POST("/operation", admin.OperationHandler.Add)
		adminAPI.PUT("/operation/:id", admin.OperationHandler.Edit)
		adminAPI.DELETE("/operation/:id", admin.OperationHandler.Delete)

		adminAPI.GET("/org", admin.OrganizationHandler.Query)
		adminAPI.GET("/org/:id", admin.OrganizationHandler.Get)
		adminAPI.POST("/org", admin.OrganizationHandler.Add)
		adminAPI.PUT("/org/:id", admin.OrganizationHandler.Edit)
		adminAPI.DELETE("/org/:id", admin.OrganizationHandler.Delete)

		adminAPI.GET("/org_options", admin.OrgToolHandler.GetSelectOptions)

		adminAPI.GET("/role", admin.RoleHandler.Query)
		adminAPI.GET("/role/:id", admin.RoleHandler.Get)
		adminAPI.POST("/role", admin.RoleHandler.Add)
		adminAPI.PUT("/role/:id", admin.RoleHandler.Edit)
		adminAPI.DELETE("/role/:id", admin.RoleHandler.Delete)

		adminAPI.GET("/subsystem", admin.SubsystemHandler.Query)
		adminAPI.GET("/subsystem/:id", admin.SubsystemHandler.Get)
		adminAPI.POST("/subsystem", admin.SubsystemHandler.Add)
		adminAPI.PUT("/subsystem/:id", admin.SubsystemHandler.Edit)
		adminAPI.DELETE("/subsystem/:id", admin.SubsystemHandler.Delete)

		adminAPI.GET("/user", admin.UserHandler.Query)
		adminAPI.GET("/user/:id", admin.UserHandler.Get)
		adminAPI.POST("/user", admin.UserHandler.Add)
		adminAPI.PUT("/user/:id", admin.UserHandler.Edit)
		adminAPI.DELETE("/user/:id", admin.UserHandler.Delete)

		adminAPI.GET("/userGetMulti", admin.UserHandler.GetMulti)

		adminAPI.GET("/user_role", admin.UserRoleHandler.Query)
		adminAPI.POST("/user_role", admin.UserRoleHandler.Add)
	}

	{
		adminOpenAPI.GET("/enum_options", admin.EnumHandler.GetOptions)
	}

	// 权限相关
	{
		adminOpenAPI.GET("/role_menu_perm", admin.RoleMenuPermHandler.Query)        // 获取指定系统 指定角色拥有的 菜单和按钮权限
		adminOpenAPI.GET("/role_menu_perm/:role_id", admin.RoleMenuPermHandler.Get) // 获取指定系统 指定角色拥有的 菜单和按钮权限
		adminOpenAPI.POST("/role_menu_perm", admin.RoleMenuPermHandler.Edit)        // 更新指定系统 指定角色拥有的权限，t_role_menu

		adminOpenAPI.GET("/menu_operation_perm", admin.MenuOperaPermHandler.Get) // 获取指定系统 的 菜单 包含按钮

		adminOpenAPI.GET("/user_opera_perm", admin.UserPermHandler.GetUserOperationIdList) // 获取 指定用户的按钮列表

		adminOpenAPI.GET("/get_sub_user_id_list", admin.OrgPermHandler.GetSubUserIdList) // 获取 下级用户Id列表
		adminOpenAPI.GET("/get_sub_user_list", admin.OrgPermHandler.GetSubUserList)      // 获取 下级用户Id列表
	}
}
